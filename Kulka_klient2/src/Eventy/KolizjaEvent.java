package Eventy;



/**
 * Klasa zdarzenia kolizji
 */

public class KolizjaEvent {

    /**
     * Zmienna określająca komendę eventu
     */
    private String actionCommand;

    /**
     * Konstruktor domyślny eventu
     * @param actionCommand Zmienna określająca komendę eventu
     *
     */
    public KolizjaEvent(String actionCommand)
    {
        this.actionCommand=actionCommand;
    }

    /**
     * Getter komendy eventu
     * @return komenda eventu
     */
    public String getActionCommand() {
        return actionCommand;
    }

}
