package Eventy;

/**
 * Klasa eventu okna glownego
 */
public class OknoGlowEvent {
    /**
     * Zmienna określająca komendę eventu
     */
    private String actionCommand;
    /**
     * Zmienna przechowujaca liczbe punktow
     */
    private int points;

    /**
     * Konstruktor eventu okna glownego
     * @param var1 Komenda Eventu
     * @param var2 Liczba puntow
     */
    public OknoGlowEvent(String var1, int var2) {
        this.actionCommand = var1;
        this.points = var2;
    }

    /**
     * Getter kmendy eventu
     * @return Komenda eventu
     */
    public String getActionCommand() {
        return this.actionCommand;
    }


    /**
     * Getter liczby punktow
     * @return Liczba punktow
     */
    public int getPoints() {
        return this.points;
    }

}
