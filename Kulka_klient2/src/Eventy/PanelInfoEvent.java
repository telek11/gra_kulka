package Eventy;


/**
 * Klasa eventu informacyjnego panelu
 */
public class PanelInfoEvent {

    /**
     * Zmienna określająca komendę eventu
     */
    private String actionCommand;

    /**
     * Ilosc uzyskanych punktów w grze
     */
    private int pointsEarned;

    /**
     * Kontruktor eventu
     * @param actionCommand komenda eventu
     * @param pointsEarned liczba zdobytych punktow
     */
    public PanelInfoEvent( String actionCommand, int pointsEarned) {
        this.actionCommand = actionCommand;
        this.pointsEarned = pointsEarned;
    }


    /**
     * Getter komendy eventu
     * @return komenda eventu
     */
    public String getActionCommand() {
        return actionCommand;
    }

    /**
     * Getter punktów zebranych w grze
     * @return liczba punktow zebranych w grze
     */

    public int getPointsEarned() {
        return pointsEarned;
    }
}
