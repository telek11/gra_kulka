package Eventy;



/**
 * Klasa zdarzeniea pola gry
 */

public class PoleGryEvent {

    /**
     * Zmienna określająca komendę eventu
     */
    private String actionCommand;

    /**
     * Czas na poziom
     */
    private int lvlTime;

    /**
     * mienna określająca współczynnik oceniania
     */
    private int wspOcen;


    /**
     * Konstruktor domyślny zdarzenia z planszy gry
     * @param actionCommand wiadmość przekazana przez event
     * @param lvlTime czas na poziom
     * @param wspO wspólczynnik ociania
     */
    public PoleGryEvent(String actionCommand, int lvlTime,int wspO){

        this.actionCommand=actionCommand;
        this.lvlTime=lvlTime;
        this.wspOcen=wspO;
    }

    /**
     * Getter komendy eventu
     * @return komenda
     */
    public String getActionCommand(){
        return actionCommand;
    }

    /**
     * Getter czasu
     * @return czas na poziom
     */
    public int getLvlTime() {return lvlTime;}

    /**
     * Getter współczynnika oceniania
     * @return Współczynnik oceniania
     */
    public int getWspOcen() {return wspOcen;}
}


