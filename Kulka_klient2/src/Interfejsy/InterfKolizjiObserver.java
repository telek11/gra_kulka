package Interfejsy;

import Eventy.KolizjaEvent;

/**
 * Interfejs obserwatora zdarzenia kolizji
 */
public interface InterfKolizjiObserver {
    /**
     * Metoda wywoływana gdy zajdzie zdarzenie zwiazane z kolizja
     * @param ActionEvent Obiekt eventu kolizji
     */
    void collisionActionPerformed(KolizjaEvent ActionEvent);
}
