package Interfejsy;



/**
 * Interfejs obiektu kolizyjnego który jest obserwowany
 */
public interface InterfKolizjiSubject {
    /**
     * Dodawanie listenerów
     * @param observer listener
     */
    void register(InterfKolizjiObserver observer);

    /**
     * Usuwanie listenerów
     * @param observer listener
     */
    void unregister(InterfKolizjiObserver observer);

    /**
     * Informowanie listenerow ze obiekt sie zmienił
     */
    void notifyObserver();
}
