package Interfejsy;

import Eventy.OknoGlowEvent;

/**
 * Interfejs obserwatora glownego okna
 */
public interface InterfOknoGlowObserver {

    /**
     * Metode informujaca ze zaszlo zdarzenie w glownym oknie
     * @param ActionEvent Obiekt eventu menu glownego
     */
    void mainWindowActionPerformed(OknoGlowEvent ActionEvent);

}
