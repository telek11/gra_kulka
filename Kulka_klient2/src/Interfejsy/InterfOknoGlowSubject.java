package Interfejsy;

import Eventy.OknoGlowEvent;




/**
 * Interfejs obiektu obserwowanego
 */

public interface InterfOknoGlowSubject {
    /**
     * Rejestracja listenerów
     * @param observer listener
     */
    void register(InterfOknoGlowObserver observer);

    /**
     * Usuwanie listenerów
     * @param observer listener
     */
    void unregister(InterfOknoGlowObserver observer);

    /**
     * Metoda informująca listenerów ze zaszło zdarzenie
     * @param ActionEvent obiekt zdarzenia okna glownego
     */
    void notifyObserver(OknoGlowEvent ActionEvent);
}
