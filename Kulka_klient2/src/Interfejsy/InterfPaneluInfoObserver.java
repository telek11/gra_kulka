package Interfejsy;

import Eventy.PanelInfoEvent;

/**
 * Interfejs obserwatora obiektu info panel
 */
public interface InterfPaneluInfoObserver {
    /**
     * Metoda informujaca ze są zmiany w obiekcie panelu info
     * @param ActionEvent Obiekt panelu informacyjnego
     */
    void infoPanelActionPerformed(PanelInfoEvent ActionEvent);
}
