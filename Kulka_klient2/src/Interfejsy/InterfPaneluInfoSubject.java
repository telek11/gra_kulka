package Interfejsy;



/**
 * Interfejs obserwowanego obiektu panelu info
 */
public interface InterfPaneluInfoSubject {
    /**
     * Rejestracja listenerów
     * @param observer listener
     */
    void register(InterfPaneluInfoObserver observer);

    /**
     * Usuwanie listenerów
     * @param observer listener
     */
    void unregister(InterfPaneluInfoObserver observer);

    /**
     * Metoda informująca listenerów ze zaszło zdarzenie
     */
    void notifyObserver();
}
