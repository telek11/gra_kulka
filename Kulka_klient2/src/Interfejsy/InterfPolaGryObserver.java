package Interfejsy;

import Eventy.PoleGryEvent;


/**
 * Interfejs obserwatora planszy gry
 */

public interface InterfPolaGryObserver {

    /**
     * Zającie zdarzenia związanego z planszą gry
     * @param ActionEvent obiekt zdarzenia panelu gry
     */

    void poleGryActionPerformed(PoleGryEvent ActionEvent);

}
