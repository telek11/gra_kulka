package Interfejsy;

import Eventy.PoleGryEvent;



/**
 * Interfejs obieektu obserwowanego planszy gry
 */

public interface InterfPolaGrySubject {
    /**
     * Rejestracja obserwatora
     * @param observer obserwator
     */
    void register(InterfPolaGryObserver observer);

    /**
     * Usuwanie obserwatorów
     * @param observer obserwator
     */

    void unregister(InterfPolaGryObserver observer);

    /**
     * Informowanie obserwatorów planszy gry
     * @param ActionEvent obiekt zdarzenia
     */

    void notifyObserver(PoleGryEvent ActionEvent);
}
