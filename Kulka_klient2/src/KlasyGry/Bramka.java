package KlasyGry;

import javax.swing.*;
import java.awt.*;

/**
 * Klasa opisująca bramkę
 */
public class Bramka extends JComponent
{
    /**
     * Polozenie x bramki
     */
    public int x;
    /**
     * Polozenie Y bramki
     */
    public int y;
    /**
     * Szerokosc bramki
     */
    public int width;
    /**
     * Wysokosc bramki
     */
    public int height;

    /**
     * Konstruktor klasy bramka
     * @param xK Polozenie x bramki
     * @param yK polozenie y bramki
     * @param widhtK szerokosc bramki
     * @param heightK wyskosc bramki
     */
    public Bramka(int xK, int yK, int widhtK, int heightK)
    {
        fillData(xK,yK,widhtK,heightK);
    }

    /**
     * Rysuje bramke
     * @param xK Polozenie x bramki
     * @param yK polozenie y bramki
     * @param widhtK szerokosc bramki
     * @param heightK wyskosc bramki
     */
    protected void fillData(int xK,int yK,int widhtK,int heightK)
    {
        x=xK;
        y=yK;
        width=widhtK;
        height=heightK;
    }

    /**
     * Metoda rysujaca bramke
     * @param g parametr graficzny
     */
    protected void paintComponent(Graphics g)
    {
        int widthParScreen= this.getParent().getWidth();
        int heightParScreen= this.getParent().getHeight();

        g.setColor(new Color(139, 194, 190));
        g.fillRect(x*widthParScreen/1000,y*heightParScreen/1000,width*widthParScreen/1000,height*heightParScreen/1000);
    }


    /**
     * Metoda zwracajaca dolno krawedz
     * @return dolna krawedz
     */
    public double getDownCordinate(){ return(y+height);}

    /**
     * Metoda zwrcajaca gorna krawedz
     * @return gorna krawedz
     */
    public double getUpCordinate(){ return(y);}

    /**
     * Metoda zwracajaca lewa krawedz
     * @return lewa krawedz
     */
    public double getLeftCordinate(){ return(x);}

    /**
     * Metoda zwracajaca prawa krawedz
     * @return prawa krawedz
     */
    public double getRightCordinate(){ return(x+width);}
}
