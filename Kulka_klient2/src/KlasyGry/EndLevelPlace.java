package KlasyGry;


import javax.swing.*;
import java.awt.*;

/**
 * Klasa opisujaca element, zderzenie z ktorym powoduje ukonczenie levelu
 */
public class EndLevelPlace extends JComponent {

    /**
     * Wsp x konca levelu
     */
    public int x;
    /**
     * wsp Y konca levelu
     */
    public int y;
    /**
     * Szerokosc kwadratu reprezentujacego koniec gry
     */
    public int width;


    /**
     * Konstruktor klasy EndLevelPlace
     * @param xK wsp x konca levelu
     * @param yK wsp y konca levelu
     * @param widhtK Szerokosc kwadratu reprezentujacego koniec gry
     */
    public EndLevelPlace(int xK, int yK, int widhtK)
    {
        fillData(xK,yK,widhtK);
    }

    /** Mtoda tworzaca kwadrat reprenzentijacy koniec levelu
     * @param xK wsp x konca levelu
     * @param yK wsp y konca levelu
     * @param widhtK Szerokosc kwadratu reprezentujacego koniec gry
     */
    private void fillData(int xK,int yK,int widhtK)
    {
        x=xK;
        y=yK;
        width=widhtK;
    }

    /**
     * Metoda rysujaca komponent
     * @param g parametr graficzny
     */
    protected void paintComponent(Graphics g)
    {
        int widthParScreen= this.getParent().getWidth();
        int heightParScreen= this.getParent().getHeight();
        g.setColor(new Color(198, 0, 178));
        g.fillRect(x*widthParScreen/1000,y*heightParScreen/1000,width*widthParScreen/1000,width*heightParScreen/1000);
    }

    /**
     * Metoda zwracajaca dolno krawedz
     * @return dolna krawedz
     */
    public double getDownCordinate(){ return(y+width);}

    /**
     * Metoda zwrcajaca gorna krawedz
     * @return gorna krawedz
     */
    public double getUpCordinate(){ return(y);}

    /**
     * Metoda zwracajaca lewa krawedz
     * @return lewa krawedz
     */
    public double getLeftCordinate(){ return(x);}

    /**
     * Metoda zwracajaca prawa krawedz
     * @return prawa krawedz
     */
    public double getRightCordinate(){ return(x+width);}

}
