package KlasyGry;

import Eventy.KolizjaEvent;
import Interfejsy.InterfKolizjiObserver;
import Interfejsy.InterfKolizjiSubject;

import java.util.ArrayList;


/**
 * Klasa odpowiedzialna za obsługę kolizji w grze
 */
public class Kolizja implements InterfKolizjiSubject {

    /**
     * Zmienna reprezentujaca plike
     */
    private Kulka kulka;
    /**
     * Zmienna reprezentujaca tablice przeszkod
     */
    private Przeszkoda[] arrPreszkod;
    /**
     * Zmienna wysokokosci ekranu
     */
    private int gameScreenHeight;
    /**
     * Zmienna szerokości ekranu
     */
    private int gameScreenWidth;
    /**
     * Tablica do przechowywania obiektów które mają być informowane po zajściu zdarzenia związanego z polem gry
     */
    private ArrayList<InterfKolizjiObserver> observers;
    /**
     * Zmienna określająca komendę eventu
     */
    private String actionCommand;
    /**
     * Zmienna przechowywujaca koniec poziomu
     */
    private EndLevelPlace endLevel;
    /**
     * Zmienna przechowywujaca przełącznik
     */
    private Przelacznik przelacznik;
    /**
     * zmienna przechowujaca bramke
     */
    private Bramka bramka;


    /**
     * Kontruktor klasy Kolizja
     * @param bl Kulka
     * @param arrPrzesz Tablica przeszkod
     * @param endLev Koniec pozimu
     * @param prze Prszelacznik
     * @param bram Bramka
     */
    public Kolizja(Kulka bl, Przeszkoda[] arrPrzesz, EndLevelPlace endLev,Przelacznik prze, Bramka bram)
    {
        observers=new ArrayList<>();
        kulka =bl;
        arrPreszkod=arrPrzesz;
        endLevel=endLev;
        bramka=bram;
        przelacznik=prze;
        actionCommand="";
    }

    /**
     *  @param arrPrzeszkod Tablica przeszkod
     * @param kulka Kulka
     * @param endLevel Koniec pozimu
     * @param przelacznik Prszelacznik
     * @param bramka Bramka
     */
    public void setNewPar(Przeszkoda[] arrPrzeszkod, Kulka kulka, EndLevelPlace endLevel, Przelacznik przelacznik, Bramka bramka)
    {
        this.kulka = kulka;
        this.arrPreszkod=arrPrzeszkod;
        this.endLevel=endLevel;
        this.przelacznik=przelacznik;
        this.bramka=bramka;
    }

    /**
     * Metoda sprwadzajaca czy jest kolizja z gorna sciana
     * @return czy wystapila kolizja z gorna sciana
     */
    private boolean topCageCollision()
    {
        boolean answ=false;
        double downCoordinate = kulka.getBallRect().getY()/1000;
        if(downCoordinate<0){answ=true;}
        return answ;//(kulka.getBallRect().getY()/1000+kulka.getBallRect().getHeight()/1000<=gameScreenHeight) ? true : false;
    }

    /**
     * Metoda sprwadzajaca czy jest kolizja z dolna sciana
     * @return czy wystapila kolizja z bolna sciana
     */
    private boolean bottomCageCollision(){
        boolean answ=false;
        double downCoordinate = kulka.getBallRect().getY()/1000+ kulka.getBallRect().getHeight()/1000;
        if(downCoordinate>gameScreenHeight){answ=true;}
        return answ;//(kulka.getBallRect().getY()+kulka.getBallRect().getHeight()>=gameScreenHeight) ? true : false;
    }

    /**
     * Metoda sprwadzajaca czy jest kolizja z lewa sciana
     * @return czy wystapila kolizja z lewa sciana
     */
    private boolean leftCageCollision(){
        return kulka.getBallRect().getX() / 1000 < 0;
    }

    /**
     * Metoda sprwadzajaca czy jest kolizja z prawa sciana
     * @return czy wystapila kolizja z gorna sciana
     */
    private boolean rightCageCollision(){
        return kulka.getBallRect().getX() / 1000 + kulka.getBallRect().getWidth() / 1000 >= gameScreenWidth;
    }

    /**
     * Metoda sprwadzajaca czy jest kolizja z przeszkodami
     * @return czy wystapila kolizja z klockami
     */
    private boolean przeszkodaCollision()
    {
        boolean answ = false;
        for(Przeszkoda pojedPrzesz : arrPreszkod)
        {
            answ = checkPrzeszColision(pojedPrzesz);
            if (answ){break;}
        }
       return answ;
    }

    /**
     * Metoda sprawdzajca czy zaszlo zderzenie z przeszkoda
     * @param pojedPrzesz pojedyncza przeszkoda
     * @return 'true' jesli zaszlo
     */
    private boolean checkPrzeszColision(Przeszkoda pojedPrzesz) {
        boolean answ = false;
        double upBallCordinate = kulka.getXYRAD("Y"); //kulka.getBallRect().getY()/1000;
        double downBallCordinate = kulka.getXYRAD("Y+R"); //kulka.getBallRect().getY()/1000+2*kulka.getBallRect().getHeight()/1000;
        double x_BallCordinate = kulka.getXYRAD("X") + kulka.getXYRAD("RAD")/2; //kulka.getBallRect().getX()/1000;
        double y_BallCordinate = kulka.getXYRAD("Y") + kulka.getXYRAD("RAD")/2; //kulka.getBallRect().getX()/1000;
        double leftBallPoint = x_BallCordinate - kulka.getXYRAD("RAD")/2;
        double rightBallPoint = x_BallCordinate + kulka.getXYRAD("RAD")/2;

        double przesUpCor= pojedPrzesz.getUpCordinate();
        double przesDowCor= pojedPrzesz.getDownCordinate();
        double przesLeftCor= pojedPrzesz.getLeftCordinate();
        double przesRightCor= pojedPrzesz.getRightCordinate();

        boolean dotykGora = upBallCordinate< przesUpCor && downBallCordinate> przesUpCor;
        boolean dotykDol = upBallCordinate< przesDowCor && downBallCordinate> przesDowCor;

        boolean znajdPmiedzy = x_BallCordinate > pojedPrzesz.getLeftCordinate() && x_BallCordinate < pojedPrzesz.getRightCordinate();

        leftBallPoint = x_BallCordinate - kulka.getXYRAD("RAD")/2;
        rightBallPoint = x_BallCordinate + kulka.getXYRAD("RAD")/2;

        boolean dotykBokPrawy =!znajdPmiedzy && (przesRightCor>leftBallPoint && rightBallPoint>przesRightCor) && (y_BallCordinate>przesUpCor && y_BallCordinate<przesDowCor);
        boolean dotykBokLewy =!znajdPmiedzy && (przesLeftCor<rightBallPoint && leftBallPoint<przesLeftCor) && (y_BallCordinate>przesUpCor && y_BallCordinate<przesDowCor);

        if(((dotykGora || dotykDol)&&( znajdPmiedzy )) || dotykBokLewy || dotykBokPrawy ){answ=true;}


        return answ;
    }

    /**
     * Metoda sprawdzajaca czy zaszlo zderzenie z bramką
     * @return 'true' jesli tak
     */
    private boolean bramkaColision() {
        boolean answ = false;
        double upBallCordinate = kulka.getXYRAD("Y"); //kulka.getBallRect().getY()/1000;
        double downBallCordinate = kulka.getXYRAD("Y+R"); //kulka.getBallRect().getY()/1000+2*kulka.getBallRect().getHeight()/1000;
        double x_BallCordinate = kulka.getXYRAD("X") + kulka.getXYRAD("RAD")/2; //kulka.getBallRect().getX()/1000;
        double y_BallCordinate = kulka.getXYRAD("Y") + kulka.getXYRAD("RAD")/2; //kulka.getBallRect().getX()/1000;
        double leftBallPoint = x_BallCordinate - kulka.getXYRAD("RAD")/2;
        double rightBallPoint = x_BallCordinate + kulka.getXYRAD("RAD")/2;

        double endLevelUpCor= bramka.getUpCordinate();
        double endLevelDowCor= bramka.getDownCordinate();
        double endLevelLeftCor= bramka.getLeftCordinate();
        double endLevelRightCor= bramka.getRightCordinate();


        boolean dotykGora = upBallCordinate< endLevelUpCor && downBallCordinate> endLevelUpCor;
        boolean dotykDol = upBallCordinate< endLevelDowCor && downBallCordinate> endLevelDowCor;
        //boolean dotykBok = leftBallPoint<przesRightCor || rightBallPoint<przesLeftCor;
        boolean znajdPmiedzy = x_BallCordinate > endLevelLeftCor && x_BallCordinate < endLevelRightCor;

        leftBallPoint = x_BallCordinate - kulka.getXYRAD("RAD")/2;
        rightBallPoint = x_BallCordinate + kulka.getXYRAD("RAD")/2;

        boolean dotykBokPrawy =!znajdPmiedzy && (endLevelRightCor>leftBallPoint && rightBallPoint>endLevelRightCor) && (y_BallCordinate>endLevelUpCor && y_BallCordinate<endLevelDowCor);
        boolean dotykBokLewy =!znajdPmiedzy && (endLevelLeftCor<rightBallPoint && leftBallPoint<endLevelLeftCor) && (y_BallCordinate>endLevelUpCor && y_BallCordinate<endLevelDowCor);


        if(((dotykGora || dotykDol)&&( znajdPmiedzy )) || dotykBokLewy || dotykBokPrawy ){answ=true;}


        return answ;
    }

    /**
     * Metoda sprawdzjaca czy zaszlo zderzenie z miejscem konca levelu
     * @return 'true' jezeli tak
     */
    private boolean endLevelCollision() {
        boolean answ = false;
        double upBallCordinate = kulka.getXYRAD("Y"); //kulka.getBallRect().getY()/1000;
        double downBallCordinate = kulka.getXYRAD("Y+R"); //kulka.getBallRect().getY()/1000+2*kulka.getBallRect().getHeight()/1000;
        double x_BallCordinate = kulka.getXYRAD("X") + kulka.getXYRAD("RAD")/2; //kulka.getBallRect().getX()/1000;
        double y_BallCordinate = kulka.getXYRAD("Y") + kulka.getXYRAD("RAD")/2; //kulka.getBallRect().getX()/1000;
        double leftBallPoint = x_BallCordinate - kulka.getXYRAD("RAD")/2;
        double rightBallPoint = x_BallCordinate + kulka.getXYRAD("RAD")/2;

        double endLevelUpCor= endLevel.getUpCordinate();
        double endLevelDowCor= endLevel.getDownCordinate();
        double endLevelLeftCor= endLevel.getLeftCordinate();
        double endLevelRightCor= endLevel.getRightCordinate();


        boolean dotykGora = upBallCordinate< endLevelUpCor && downBallCordinate> endLevelUpCor;
        boolean dotykDol = upBallCordinate< endLevelDowCor && downBallCordinate> endLevelDowCor;
        //boolean dotykBok = leftBallPoint<przesRightCor || rightBallPoint<przesLeftCor;
        boolean znajdPmiedzy = x_BallCordinate > endLevelLeftCor && x_BallCordinate < endLevelRightCor;

        leftBallPoint = x_BallCordinate - kulka.getXYRAD("RAD")/2;
        rightBallPoint = x_BallCordinate + kulka.getXYRAD("RAD")/2;

        boolean dotykBokPrawy =!znajdPmiedzy && (endLevelRightCor>leftBallPoint && rightBallPoint>endLevelRightCor) && (y_BallCordinate>endLevelUpCor && y_BallCordinate<endLevelDowCor);
        boolean dotykBokLewy =!znajdPmiedzy && (endLevelLeftCor<rightBallPoint && leftBallPoint<endLevelLeftCor) && (y_BallCordinate>endLevelUpCor && y_BallCordinate<endLevelDowCor);


        if(((dotykGora || dotykDol)&&( znajdPmiedzy )) || dotykBokLewy || dotykBokPrawy ){answ=true;}


        return answ;
    }

    /**
     * sprawdzenie czy nastopila kolizja z przelącznikiem
     * @return 'true' jezeli tak
     */
    private boolean przelacznikColision() {

        boolean answ = false;
        double upBallCordinate = kulka.getXYRAD("Y"); //kulka.getBallRect().getY()/1000;
        double downBallCordinate = kulka.getXYRAD("Y+R"); //kulka.getBallRect().getY()/1000+2*kulka.getBallRect().getHeight()/1000;
        double x_BallCordinate = kulka.getXYRAD("X") + kulka.getXYRAD("RAD")/2; //kulka.getBallRect().getX()/1000;
        double y_BallCordinate = kulka.getXYRAD("Y") + kulka.getXYRAD("RAD")/2; //kulka.getBallRect().getX()/1000;
        double leftBallPoint = x_BallCordinate - kulka.getXYRAD("RAD")/2;
        double rightBallPoint = x_BallCordinate + kulka.getXYRAD("RAD")/2;

        double endLevelUpCor= przelacznik.getUpCordinate();
        double endLevelDowCor= przelacznik.getDownCordinate();
        double endLevelLeftCor= przelacznik.getLeftCordinate();
        double endLevelRightCor= przelacznik.getRightCordinate();

        boolean dotykGora = upBallCordinate< endLevelUpCor && downBallCordinate> endLevelUpCor;
        boolean dotykDol = upBallCordinate< endLevelDowCor && downBallCordinate> endLevelDowCor;
        //boolean dotykBok = leftBallPoint<przesRightCor || rightBallPoint<przesLeftCor;
        boolean znajdPmiedzy = x_BallCordinate > endLevelLeftCor && x_BallCordinate < endLevelRightCor;

        leftBallPoint = x_BallCordinate - kulka.getXYRAD("RAD")/2;
        rightBallPoint = x_BallCordinate + kulka.getXYRAD("RAD")/2;

        boolean dotykBokPrawy =!znajdPmiedzy && (endLevelRightCor>leftBallPoint && rightBallPoint>endLevelRightCor) && (y_BallCordinate>endLevelUpCor && y_BallCordinate<endLevelDowCor);
        boolean dotykBokLewy =!znajdPmiedzy && (endLevelLeftCor<rightBallPoint && leftBallPoint<endLevelLeftCor) && (y_BallCordinate>endLevelUpCor && y_BallCordinate<endLevelDowCor);


        if(((dotykGora || dotykDol)&&( znajdPmiedzy )) || dotykBokLewy || dotykBokPrawy ){answ=true;}


        return answ;

    }



    /**
     * Metoda odpowiedzialna za sprawdzanie kolizji w grze
     * @param gameScreenWidth szerokosc panelu
     * @param gameScreenHeight wysokosc panelu
     */
    public void checkCollisions(int gameScreenWidth, int gameScreenHeight)
    {
        this.gameScreenHeight=gameScreenHeight;
        this.gameScreenWidth=gameScreenWidth;

        if(bottomCageCollision() || rightCageCollision() || leftCageCollision() || topCageCollision())
        {
            actionCommand="BallOutOfGameScreen";
            notifyObserver();
        }

        if(przeszkodaCollision())
        {
            actionCommand="PrzeszkodaCollision";
            notifyObserver();
        }

        if(endLevelCollision())
        {
            actionCommand="EndLevelCollision";
            notifyObserver();
        }
       if(przelacznikColision())
        {
            actionCommand="PrzelacznikColision";
            notifyObserver();
        }
        if(bramkaColision())
        {
            actionCommand="BramkaColision";
            notifyObserver();
        }

    }


    /**
     * rejestracja listenera
     * @param observer listener
     */
    @Override
    public void register(InterfKolizjiObserver observer) {
        observers.add(observer);
    }

    /**
     * usuwanie listenera
     * @param observer listener
     */
    @Override
    public void unregister(InterfKolizjiObserver observer) {
        observers.remove(observers.indexOf(observer));
    }

    /**
     * informowanie listenerow ze zaszlo zdarzenie
     */
    @Override
    public void notifyObserver()
    {
        KolizjaEvent ActionEvent=new KolizjaEvent(actionCommand);
        for(InterfKolizjiObserver observer : observers){
            observer.collisionActionPerformed(ActionEvent);
        }
    }

}
