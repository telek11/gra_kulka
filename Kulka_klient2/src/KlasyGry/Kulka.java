package KlasyGry;

import Eventy.KolizjaEvent;
import Interfejsy.InterfKolizjiObserver;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;



/**
 * Klasa opisująca piłkę i jej zachowanie
 */
public class Kulka extends JComponent implements InterfKolizjiObserver, KeyListener {

    /**
     * Typy poruszan pilki
     */
    private typeOfMove ballState;

    /**
     * Zmienna wyliczeniowa określająca stan w którym zanjduje się paletka
     */
    private enum typeOfMove {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    /**
     * Poczatkwa wspolrzedna x
     */
    private double startedX;
    /**
     * Poczatkwa wspolrzedna y
     */
    private double startedY;

    /**
     * Zmienna opisująca piłkę jako kwadrat
     */
    private Rectangle ballRect;
    /**
     * Zmienna opisująca ilośc FPS w grze
     */
    private int FPS;
    /**
     * Zmienna określająca szerokość planszy do gry
     */
    private int screenWidth;
    /**
     * Zmienna określająca wysokość planszy do gry
     */
    private int screenHeight;
    /**
     * Znormalizowana prędkość piłki w kierunku X
     */
    private double normalizedXVelocity;
    /**
     * Znormaliwana prędkość w kierunku Y
     */
    private double normalizedYVelocity;
    /**
     * Znormalizowana wartość wypadkowa prędkości
     */
    private double normalizedVelocityVectorLength;
    /**
     * Znormalizowany promień piłki
     */
    private double normalizedBallRadius;

    /**
     * Znormalizowana pozycja X piłki
     */
    private double normalizedXPosition;
    /**
     * Znormalizowana pozycja Y piłki
     */
    private double normalizedYPosition;

    /**
     * Konstruktor klasy Kulka
     * @param screenWidth Parametr szerokości ekranu do którego ma się dostosować piłka
     * @param screenHeight Parametr wysokości ekranu do ktorego ma się dostosować piłka
     * @param FPS Parametr ilości FPS według którego skalować się będzie prędkość ruchu piki
     */
    public Kulka(int screenWidth, int screenHeight, int FPS){
        this.FPS=FPS;
        this.screenHeight=screenHeight;
        this.screenWidth=screenWidth;

        normalizedBallRadius=70;

        normalizedXVelocity=0;
        normalizedYVelocity=0;
        normalizedVelocityVectorLength=Math.sqrt(normalizedXVelocity*normalizedXVelocity+normalizedYVelocity*normalizedYVelocity);
        ballRect = new Rectangle();
        ballRect.setLocation((int)Math.round(normalizedXPosition*screenWidth),(int)Math.round(normalizedYPosition*screenHeight));
        ballRect.setSize((int)Math.round(normalizedBallRadius*screenHeight),(int)Math.round(normalizedBallRadius*screenHeight));

        setStartPosition();
    }

    /**
     * metoda rysujaca kulke
     * @param g parametr graficzny
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        double x =normalizedXPosition;
        double y =normalizedYPosition;
        double radius = normalizedBallRadius;

        int widthParScreen= this.getParent().getWidth();
        int heightParScreen= this.getParent().getHeight();

        g.setColor(Color.BLUE);
        g.fillOval((int)(x*widthParScreen/1000),(int)(y*heightParScreen/1000)
                ,(int)(radius*widthParScreen/1000),(int)(radius*heightParScreen/1000));
    }

    /**
     * Metoda ustawiajaca kulke w polozeniu startowym
     * oraz ustawiajaca jej predkosc na 0
     */
    public void setStartPosition()
    {
       normalizedXPosition=startedX;
       normalizedYPosition=startedY;
       normalizedYVelocity = 0;
       normalizedXVelocity = 0;
    }

    /**
     *  Metoda ustawiajaca parametry startowego polozneia kulki
     * @param sX startowy x kulki
     * @param sY startowy y kulki
     */
    public void changeStartPos(double sX,double sY){
        startedX=sX;
        startedY=sY;
        setStartPosition();
    }
    /**
     * Metoda zwracająca kwadrat reprezentujący piłkę
     * @return Zwraca prostokąt reprezentujący piłkę
     */
    public Rectangle getBallRect(){
        return ballRect;
    }

    /**
     * Metoda zwracajaca unormowane polozenie kulki
     * @param wiel
     * @return
     */
    public double getXYRAD(String wiel){
        switch (wiel){
            case "X": return normalizedXPosition;
            case "Y": return normalizedYPosition;
            case "Y+R": return normalizedYPosition+70;
            case "RAD": return 70;
            default : return  normalizedXPosition;
        }
    }

    /**
     * Metoda uaktualniająca ruch piłki
     */
    public void update() {

        if(normalizedXVelocity>0){normalizedXVelocity-=3;}// PO CO TO ?
        if(normalizedXVelocity<0){normalizedXVelocity+=3;}// PO CO TO ?
        if(normalizedYVelocity<200){normalizedYVelocity+=40;}// PO CO TO ?

        normalizedXPosition = normalizedXPosition + normalizedXVelocity / FPS;
        normalizedYPosition = normalizedYPosition + normalizedYVelocity / FPS;

        ballRect.setLocation((int)Math.round(normalizedXPosition*screenWidth),(int)Math.round(normalizedYPosition*screenHeight));
        ballRect.setSize((int)Math.round(normalizedBallRadius*screenHeight),(int)Math.round(normalizedBallRadius*screenHeight));
    }

    /**
     * Metoda poruszajaca pilke nadajac jej predkosc
     */
    private void MoveBall() {

        if (ballState == typeOfMove.LEFT) {
            normalizedXVelocity -= 200;
         }

        if (ballState == typeOfMove.RIGHT) {
            normalizedXVelocity += 200;
        }

        if (ballState == typeOfMove.UP) {
            normalizedYVelocity -= 550;
        }

        if (ballState == typeOfMove.DOWN) {
            normalizedYVelocity += 550;
        }

    }

    /**
     * Metoda informująca ze zaszlo zdarzenie kolizji
     * @param ActionEvent Obiekk eventu kolizji
     */
    @Override
    public void collisionActionPerformed(KolizjaEvent ActionEvent) {

            String actionCommand=ActionEvent.getActionCommand();
            switch (actionCommand)
            {
                case "PrzeszkodaCollision":
                    System.out.println("Uderzenie o przeszkode");
                    setStartPosition();
                    break;

                case "BramkaColision":
                    System.out.println("uderzenie w bramke");
                    setStartPosition();
                    break;

                case "BallOutOfGameScreen":
                    System.out.println("Za ekranem");
                    setStartPosition();
                    break;

                case "EndLevelCollision":
                    System.out.println("Koniec lev,pilka wraca");
                    setStartPosition();
                    break;

                case "PrzelacznikColision":

                    break;

                default:
                    break;
            }
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.print("");
        int keyCode = e.getKeyCode();
        if(keyCode== KeyEvent.VK_LEFT)
        {
            setMovementState(typeOfMove.LEFT);
            MoveBall();
        }
        if(keyCode== KeyEvent.VK_RIGHT)
        {
            setMovementState(typeOfMove.RIGHT);
            MoveBall();
        }
        if(keyCode== KeyEvent.VK_UP)
        {
            setMovementState(typeOfMove.UP);
            MoveBall();
        }
        if(keyCode== KeyEvent.VK_DOWN)
        {
            setMovementState(typeOfMove.DOWN);
            MoveBall();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {}

    /**
     * metoda ustawiajaca stan pilki
     * @param state stan w ktorym ma sie znajdowac pilka
     */
    private void setMovementState(typeOfMove state){
        ballState=state;
    }



}
