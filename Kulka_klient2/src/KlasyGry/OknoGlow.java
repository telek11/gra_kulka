package KlasyGry;

import Stale.Stale;
import Eventy.KolizjaEvent;
import Eventy.PanelInfoEvent;
import Eventy.OknoGlowEvent;
import Interfejsy.InterfKolizjiObserver;
import Interfejsy.InterfPaneluInfoObserver;
import Interfejsy.InterfOknoGlowObserver;
import Interfejsy.InterfOknoGlowSubject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Klasa opisujaca glowne okno w ktorym rysowana jest plansza do gry i panel informacyjny
 */

public class OknoGlow extends JFrame implements ComponentListener, ActionListener, InterfKolizjiObserver, InterfPaneluInfoObserver, InterfOknoGlowSubject {

    /**
     * Licznik czasu
     */
    private Timer timer;
    /**
     * Szerokość okna
     */
    private int frameWidth=500;
    /**
     * Wysokość okna
     */
    private int frameHight=678;
    /**
     * Obiekt panelu informacyjnego
     */
    private PanelInfo panelInfo;
    /**
     * Obiekt planszy do gry
     */
    private PoleGry poleGryPanel;
    /**
     * Listener menu głownego
     */
    private ActionListener menuListener;
    /**
     * Wątek gry obliczajacy ruchy i zderzenia
     */
    private Thread GST;
    /**
     * Obraz na który nakładana jest grafika do DB
     */
    private Image dbImage;
    /**
     * Grafika do której wrzucane sa wszystkie elementy
     */
    private Graphics dbGfx;
    /**
     * Rozmiar okna
     */
    private Dimension FrameSize ;
    /**
     * Kolor tła
     */
    private Color FrameColor= Color.lightGray;
    /**
     * Stany gry
     */
    private gameStates gameState;

    /**
     * Tablica do przechowywania obiektów które mają być informowane po zajściu zdarzenia związanego z polem gry
     */
    private ArrayList<InterfOknoGlowObserver> observers;

    /**
     * Czy nacisniety przycisk myszki podczas skalowania
     */
    private boolean isMouseButtonPressed;

    /**
     * Konstrukor klasy OknoGlow
     * @param menuList Listener menu głownego
     * @param highScore najlepszy wynik
     * @param serverSocket socket servera
     */
    public OknoGlow(ActionListener menuList, int highScore, Socket serverSocket){

        gameState=gameStates.PAUSED;
        menuListener=menuList;
        setDefaultFrameParameters();
        initializeUI(highScore, serverSocket);
        initializeTimer();
        pack();
        addComponentListener(this);
        observers = new ArrayList<>();

        poleGryPanel.addListeners(panelInfo, this);
        panelInfo.register(this);
        setActionListenerInPanelInfo();
        poleGryPanel.register(panelInfo);
        GST= new Thread(poleGryPanel);
        GST.start();

        pause();
    }

    /**
     * Inicializator domyślmych parametrów okna
     */
    private void setDefaultFrameParameters(){
        setTitle(Stale.napisOknaGlow);
        FrameSize= new Dimension(frameHight,frameWidth);
        getContentPane().setPreferredSize(FrameSize);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBackground(FrameColor);
        setTitle("Gra_Kulka");
        setLayout(new BorderLayout());

    }

    /**
     * Inicializator interfejsu graficznego
     * @param highScore najwyzszy wynik
     * @param serverSocket socketServera
     */
    private void initializeUI(int highScore, Socket serverSocket){
        isMouseButtonPressed = false;
        panelInfo =new PanelInfo(frameHight,frameWidth,Color.red, this, highScore);
        //panelInfo.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED), BorderFactory.createEtchedBorder(EtchedBorder.LOWERED)));
        this.getContentPane().add(panelInfo,BorderLayout.EAST);
        poleGryPanel =new PoleGry(frameWidth,frameHight,serverSocket);
        getContentPane().add(poleGryPanel,BorderLayout.CENTER);

    }

    /**
     * Inicializator timera
     */
    private void initializeTimer(){
        timer= new Timer(1000, panelInfo);
        timer.setActionCommand("TimerTic");



    }

    /**
     * Metoda obsługująca zdarzenia kolizji
     * @param ActionEvent Obiekk eventu kolizji
     */
    @Override
    public void collisionActionPerformed(KolizjaEvent ActionEvent) {

        String actionCommand=ActionEvent.getActionCommand();
        switch (actionCommand){
            case "BallOutOfGameScreen":
                //pause();
                break;
            case "EndLevelCollision":
               // pause();

                break;
            case "NewLevelLoaded":
                //pause();

                break;
            default:
                break;

        }
    }

    /**
     * Metoda obsługująca zdarzenia panelu informacyjnego
     * @param ActionEvent Obiekt panelu informacyjnego
     */
    @Override
    public void infoPanelActionPerformed(PanelInfoEvent ActionEvent) {
        String actionCommand=ActionEvent.getActionCommand();
        switch (actionCommand){
            case "TimeOver":
                endGame(Stale.napisUplynalCzas,ActionEvent.getPointsEarned());
                break;
            case "LostAllLifes":
                endGame(Stale.napisSkonczylySieZycia,ActionEvent.getPointsEarned());
                break;

            case "NoMoreLevelsAvailable":
                endGame(Stale.napisKoniecGryInfo,ActionEvent.getPointsEarned());
                break;
            default:
                break;
        }
    }

    /**
     * Rejestracja obserwatór panelu obserwacyjnego
     * @param observer listener
     */

    @Override
    public void register(InterfOknoGlowObserver observer) {
        observers.add(observer);
    }

    /**
     * Usuwanie obserwatorów planszy do gry
     * @param observer listener
     */


    @Override
    public void unregister(InterfOknoGlowObserver observer) {
        observers.remove(observers.indexOf(observer));
    }

    /**
     * Informowanie obserwatorów o zajściu zdarzenia
     * @param ActionEvent obiekt zdarzenia glownego okna gry
     */

    @Override
    public void notifyObserver(OknoGlowEvent ActionEvent) {
        for(InterfOknoGlowObserver observer : observers){
            observer.mainWindowActionPerformed(ActionEvent);
        }
    }



    /**
     * Możliwe stany gry
     */
    private enum gameStates {
        PAUSED,
        RUNNING,
        OVER
    }


    /**
     * Metoda służąca do zatrzymania gry
     */

    public void pause(){
        if(gameState==gameStates.PAUSED)
        {
            gameState=gameStates.RUNNING;
            startTimer();


            //this.requestFocus();
        }
        else{
            gameState=gameStates.PAUSED;
            stopTimer();
        }
        System.out.println(gameState);
        poleGryPanel.pause();
        poleGryPanel.requestFocus();
    }

    /**
     * Metoda konczoca gre
     * @param endType powod zakonczenia
     * @param pointsEarned liczba zdobytych punktow
     */
    private void endGame(String endType,int pointsEarned){
        gameState=gameStates.OVER;
        timer.stop();
        poleGryPanel.gameOver();
        Object[] options={
                "Graj ponownie", Stale.napisNaPrzycPowrDoMenuGlow, Stale.napisNajlepWyn
        };
        switch(JOptionPane.showOptionDialog(this, Stale.napisKoniecGry +","+ endType+" \n" + Stale.napisLiczbePkt + pointsEarned, Stale.napisKoniecGryInfo
                ,JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,null,options,options[1])) {
            case JOptionPane.YES_OPTION:
                notifyObserver(new OknoGlowEvent("PlayAgain", panelInfo.getAmountOfPoints()));
                break;
            case JOptionPane.NO_OPTION:
                notifyObserver(new OknoGlowEvent("BackToMenu", panelInfo.getAmountOfPoints()));
                break;
            case JOptionPane.CANCEL_OPTION:
                notifyObserver(new OknoGlowEvent("ShowHighScores", panelInfo.getAmountOfPoints()));
                break;
            default:
                break;
        }
    }


    /**
     * Metoda odpowiedzialna za rysownaie okna głównego i komponentów które się w tym oknie znajdują
     * @param gfx grafika
     */
    @Override
    public void paint(Graphics gfx)
    {
        dbImage=createImage(getWidth(),getHeight());
        dbGfx = dbImage.getGraphics();
        super.paint(dbGfx);
        draw(dbGfx);
        gfx.setColor(Color.black);
        gfx.drawImage(dbImage,0,0,this);
    }

    /**
     * Metoda pomocnicza do odrysowywania
     * @param g grafika
     */
    private void draw(Graphics g){
        repaint();
    }

    /**
     * Metoda opisująca zachowanie okna gdy zmieni się jego rozmiar
     * @param componentEvent obiekt zdarzenia komponentu graficznego
     */
    @Override
    public void componentResized(ComponentEvent componentEvent) {
        if(!isMouseButtonPressed){

            panelInfo.autoResize(getContentPane().getWidth(), getContentPane().getHeight());
            poleGryPanel.autoResize(getContentPane().getWidth(), getContentPane().getHeight());
        }
    }


    /**
     * Metoda opisujaca zachowanie okna gdy zmieni się położenie
     * @param componentEvent obiekt zdarzenia komponentu graficznego
     */
    @Override
    public void componentMoved(ComponentEvent componentEvent) {
    }
    /**
     * Metoda opisująca zachowanie okna gdy zostanie odkryte
     * @param componentEvent obiekt zdarzenia komponentu graficznego
     */
    @Override
    public void componentShown(ComponentEvent componentEvent) {

    }
    /**
     * Metoda opisująca zachowanie okna gdy zostanie schowane
     * @param componentEvent obiekt zdarzenia komponentu graficznego
     */
    @Override
    public void componentHidden(ComponentEvent componentEvent) {

    }

    /**
     * Obsługa zdarzenia generowananego przez przycisk Pause w panelu info
     * @param actionEvent obiekt zdarzenia
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String cmd = actionEvent.getActionCommand();
        switch (cmd) {
            case "PauseButton":
                pause();
                break;
            default:
                break;
        }


    }



    /**
     * Metoda rozpoczynająca pomiar czasu
     */
    public void startTimer(){
        initializeTimer();
        timer.start();
    }

    /**
     * Metoda kończąca odliczanie czasu
     */
    private void stopTimer(){
        timer.stop();
    }


    /**
     * Setter słuchacza okna glownego
     */
    public void setActionListenerInPanelInfo(){
        panelInfo.setOknGlwActListner(this);
    }

}
