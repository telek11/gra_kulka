package KlasyGry;

import Eventy.OknoGlowEvent;
import Interfejsy.InterfOknoGlowObserver;
import Stale.Stale;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

/**
 * Klasa opisujaca glowne menu
 */
public class OknoMenu extends JFrame implements ActionListener, InterfOknoGlowObserver {

    /**
     * Panel z wynikami
     */
    private WynikiPanel panelWynikow;
    /**
     * Socket servera
     */
    private Socket serverSocket;
    /**
     * Zmienna określająca czy gra jest w trybie offline czy online
     */
    private boolean online;
    /**
     * Rozmiar okna menu
     */
    private Dimension frameSize;
    /**
     * Panel opisujacy menu
     */
    private JPanel menuPan;
    /**
     *Napis Z nazwa gry
     */
    private JLabel titleLabel;
    /**
     * Przycisk Nowa Gra
     */
    private JButton newGameButton;
    /**
     * Przycisk Najlepsze wyniki
     */
    private JButton highScoresButton;
    /**
     * przycisk Ustawien
     */
    private JButton optionsButton;
    /**
     * przycisk Wyjscie
     */
    private JButton exitButton;
    /**
     * Nazwa gracza
     */
    private String nickName;
    /**
     * Obiekt Okna gry
     */
    private OknoGlow oknoGlow;
    /**
     * Panel Informacji
     */
    private Informacje informacje;
    /**
     * Liczba punktow
     */
    private int highScore;


    /**
     *Konstruktor Klasy OknoMenu
     * @param size rozmiary okna
     * @param socket socket serwera
     */
    public OknoMenu(Dimension size, Socket socket) {
        this.serverSocket=socket;
        frameSize = size;
        setDefaultProperties();
        initializeUI();
        online = serverSocket != null;
    }

    /**
     * Inicializator interfejsfu graficznego
     */

    private void initializeUI() {

        menuPan = new JPanel();
        menuPan.setLayout(new BoxLayout(menuPan,BoxLayout.Y_AXIS));
        //menuPan.setBackground(new Color(222));

        JPanel pan1 = new JPanel();
        JPanel pan2 = new JPanel();
        JPanel pan3 = new JPanel();
        JPanel pan4 = new JPanel();
        JPanel pan5 = new JPanel();

        pan1.setPreferredSize(new Dimension(200,100));
        pan2.setPreferredSize(new Dimension(200,100));
        pan3.setPreferredSize(new Dimension(200,100));
        pan4.setPreferredSize(new Dimension(200,100));
        pan5.setPreferredSize(new Dimension(200,100));

        pan1.setLayout(new BoxLayout(pan1,BoxLayout.LINE_AXIS));
        pan2.setLayout(new BoxLayout(pan2,BoxLayout.LINE_AXIS));
        pan3.setLayout(new BoxLayout(pan3,BoxLayout.LINE_AXIS));
        pan4.setLayout(new BoxLayout(pan4,BoxLayout.LINE_AXIS));
        pan5.setLayout(new BoxLayout(pan5,BoxLayout.LINE_AXIS));

        titleLabel= new JLabel("KULKA");
        titleLabel.setFocusable(false);

        newGameButton=new JButton("Nowa gra");
        newGameButton.setActionCommand("NewGame");
        newGameButton.setFocusable(false);
        newGameButton.addActionListener(this);

        highScoresButton=new JButton("Najlepsze wyn");
        highScoresButton.setActionCommand("HighScores");
        highScoresButton.setFocusable(false);
        highScoresButton.addActionListener(this);

        optionsButton=new JButton("Informacje");
        optionsButton.setActionCommand("Inform");
        optionsButton.setFocusable(false);
        optionsButton.addActionListener(this);

        exitButton=new JButton("Wyjscie");
        exitButton.setActionCommand("Exit");
        exitButton.setFocusable(false);
        exitButton.addActionListener(this);

        pan1.add(titleLabel);
        pan2.add(newGameButton);
        pan3.add(highScoresButton);
        pan4.add(optionsButton);
        pan5.add(exitButton);

        menuPan.add(pan1);
        menuPan.add(pan2);
        menuPan.add(pan3);
        menuPan.add(pan4);
        menuPan.add(pan5);

        menuPan.setPreferredSize(frameSize);

/*        menuPan.add(newGameButton);
        menuPan.add(loadGameButton);
        menuPan.add(highScoresButton);
        menuPan.add(optionsButton);
        menuPan.add(exitButton);

        menuPan.setPreferredSize(frameSize);
        menuPan.setLayout(new GridLayout(5,1));*/
        getContentPane().add(menuPan);
    }

    /**
     * Inicializator domyślnych parametrów
     */
    private void setDefaultProperties() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(frameSize);
        setTitle("Kulka");

        this.setLocationRelativeTo(null);
        pack();
    }

    /**
     * Metoda obsługjąca zdarzenia wciśnięcia przycisków w menu
     * @param actionEvent obiekt zdarzenia akcji zwiazanych z wcisnieciem przycisku
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String command = actionEvent.getActionCommand();
        switch (command){
            case "NewGame":
                this.setVisible(false);
                while(nickName ==null || nickName.length()==0){
                    nickName = JOptionPane.showInputDialog(this,"Prosze podac imie/nick","Nowy gracz",JOptionPane.INFORMATION_MESSAGE);
                    if(nickName ==null){
                        break;
                    }
                }
                if(nickName ==null) {
                    this.setVisible(true);
                    break;
                }
                makeOknoGry();
                break;
            case "Kontynuj":

                break;
            case "HighScores":
                panelWynikow = new WynikiPanel((int) frameSize.getWidth(), (int) frameSize.getHeight(), this, online, serverSocket);
                this.remove(menuPan);
                this.add(panelWynikow);
                this.revalidate();
                this.repaint();
                break;
            case "Inform":
                informacje = new Informacje((int) frameSize.getWidth(), (int) frameSize.getHeight(), this, online, serverSocket);
                this.remove(menuPan);
                this.add(informacje);
                this.revalidate();
                this.repaint();
                break;
            case "Exit":
                Object[] options = {"Tak","Nie"};
                int reply = JOptionPane.showOptionDialog(this, "Czy chesz wyjsc?", "Na pewno?",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
                if (reply == JOptionPane.YES_OPTION) {
                    try {
                        if (serverSocket != null)
                            serverSocket.close();
                        System.exit(0);
                    } catch (IOException e) {
                        //System.out.println("Połączenie nie mogło zostać zakończone");
                        System.err.println(e);
                    }
                    break;
                }
                break;
            case "BackToMenuFromInformacje":
                this.remove(informacje);
                this.add(menuPan);
                this.revalidate();
                this.repaint();
                break;
            case "BackToMenuFromScorePanel":
                this.remove(panelWynikow);
                this.add(menuPan);
                this.revalidate();
                this.repaint();
                break;
            default :
                break;
        }

    }

    /**
     * Inicjalizator okna gry
     */
    public void makeOknoGry(){
        if (oknoGlow == null) {
            oknoGlow = new OknoGlow(this, highScore , serverSocket);
            oknoGlow.setVisible(true);
            oknoGlow.register(this);
        } else {
            oknoGlow.dispose();
            oknoGlow = new OknoGlow(this, highScore, serverSocket);
            oknoGlow.setVisible(true);
            oknoGlow.register(this);
        }

    }

    /**
     * Rozwiązanie zajścia zdarzenia z obiektem menu głownego
     * @param ActionEvent Obiekt menu glownego
     */
    @Override
    public void mainWindowActionPerformed(OknoGlowEvent ActionEvent)
    {
        String actionCommand=ActionEvent.getActionCommand();
        switch (actionCommand){
            case "PlayAgain":
                //System.out.println("wybrano play again");

                panelWynikow=new WynikiPanel(serverSocket,online);
                panelWynikow.addScore(nickName,ActionEvent.getPoints());
                oknoGlow.setVisible(false);
                this.remove(oknoGlow);
                makeOknoGry();
               break;
            case "BackToMenu":
                panelWynikow=new WynikiPanel(serverSocket,online);
                panelWynikow.addScore(nickName,ActionEvent.getPoints());
                oknoGlow.setVisible(false);
                this.remove(oknoGlow);
                StartGry.makeGlowMenu(serverSocket);
                break;
            case "ShowHighScores":
                panelWynikow=new WynikiPanel(serverSocket,online);
                panelWynikow.addScore(nickName,ActionEvent.getPoints());
                panelWynikow = new WynikiPanel((int) frameSize.getWidth(), (int) frameSize.getHeight(), this, online, serverSocket);
                highScore=panelWynikow.getHighScore();
                oknoGlow.setVisible(false);
                this.remove(oknoGlow);
                this.remove(menuPan);
                this.add(panelWynikow);
                this.revalidate();
                this.repaint();
                this.setVisible(true);
                break;
        }
    }

    /**
     * Klasa panelu informacji
     */
    private class Informacje extends  JPanel {
        /**
         * Tytul
         */
        private String title;
        /**
         * Tablica z zasadami gry
         */
        private ArrayList<String> zasday;

        /**
         * KONSTRUKTOR PANELU INFORMACJI
         * @param width szerokosc pannelu
         * @param height wysokosc panelu
         * @param menuListener sluchacz(listener) panelu glownego
         * @param online socket servera
         */
        public Informacje (int width, int height, ActionListener menuListener, boolean online, Socket serverSocket)
        {
            setLayout(new BorderLayout());
            setPreferredSize(new Dimension(width, height));
            zasday = new ArrayList<>();
            if(online)
            {
                getInformacjeFromServer(serverSocket); // jezeli gra w trybie online to wywolywana funkcja pobierajaca informacje z serwera
            }                                          // ktora pobiera informacje o grze z serwera i zapisuje je w lokalnyym pliku
            else
            {
                loadFromFile(); // wczytanie z lokalnego pliku
            }

            add(stworzEtykieteInformacji(), BorderLayout.CENTER);
            add(stworzPrzyciskPowrotu(menuListener), BorderLayout.SOUTH);
        }
        /**
         *Parser Informacji
         */
        private void loadFromFile()
        {
            try {
                File xmlInputFile = new File("PlikiKonfiguracyjne//rules.xml");

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(xmlInputFile);
                doc.getDocumentElement().normalize();                       // Wykorzytsanie Parsera DOM
                NodeList nList = doc.getElementsByTagName("title");
                title = nList.item(0).getTextContent();
                nList = doc.getElementsByTagName("rules");
                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        zasday.add(eElement.getTextContent());
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                System.out.println(e.toString());
                JOptionPane.showMessageDialog(this, "Nie moszna polaczyc sie z serwerem", "Blad", JOptionPane.ERROR_MESSAGE);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        /**
         * Pobranie Informacji z serwera
         * @param serverSocket socket servera
         */
        private void getInformacjeFromServer(Socket serverSocket)
        {
            try {
                serverSocket.getInputStream().skip(serverSocket.getInputStream().available()); // Znieszczenie(pominiecie) czegokolwiek co bylo w buforze
                OutputStream os = serverSocket.getOutputStream();
                PrintWriter pw = new PrintWriter(os, true);
                pw.println("Pobierz_Informacje"); // wysyla do serwera komende "Pobierz informacje
                InputStream is = serverSocket.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String settings = br.readLine();
                if(settings!="Niepoprawna_Komenda" && settings!=null)  // jezeli serwer nie zwrocil iniepoprawna komenda lub wartosci pustej to odbiera informacje o grze od serwera
                {                                                       // i zapisuje je w pliku lokalnym zawierajacym zasady gry
                    try {

                        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                        Document doc = dBuilder.parse(new InputSource(new StringReader(settings))); //Parsowanie stringa zawierajcaego odpoweidz serwera
                        doc.getDocumentElement().normalize();                       // Wykorzytsanie Parsera DOM

                        NodeList nList = doc.getElementsByTagName("title");
                        title = nList.item(0).getTextContent();
                        nList = doc.getElementsByTagName("rules");
                        for (int temp = 0; temp < nList.getLength(); temp++) {
                            Node nNode = nList.item(temp);
                            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                                Element eElement = (Element) nNode;
                                zasday.add(eElement.getTextContent());
                            }
                        }
                    }
                    catch (FileNotFoundException e)
                    {
                        System.out.println(e.toString());
                        JOptionPane.showMessageDialog(this, "Nie moszna polaczyc sie z serwerem", "Blad", JOptionPane.ERROR_MESSAGE);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            catch (IOException e)
            {
                System.out.println("PLik nie mógł zostać pobrany z serwera");
                System.out.println(e);
            }
        }
        /**
         * Generator etykiety Informajci
         * @return etykieta Informacji
         */
        private JLabel stworzEtykieteInformacji() {
            StringBuilder strB = new StringBuilder("<html><h1>");
            strB.append(title + "</h1><br>");
            int counter = 1;
            for (String rule : zasday) {
                strB.append(counter + ". " + rule + "<br>");
                counter++;
            }
            strB.append("</htm>");
            String labelText = strB.toString();
            JLabel rulesLabel = new JLabel(labelText);
            return rulesLabel;
        }

        /**
         * Generator przycisku powrotu do menu głownego
         * @param menuListener listener menu głownego
         * @return przycisk powrotu do menu
         */
        private JButton stworzPrzyciskPowrotu(ActionListener menuListener) {
            JButton backToMainMenuBtn = new JButton(Stale.napisNaPrzycPowrDoMenuGlow);
            backToMainMenuBtn.setFocusable(false);
            backToMainMenuBtn.addActionListener(menuListener);
            backToMainMenuBtn.setActionCommand("BackToMenuFromInformacje");
            return backToMainMenuBtn;
        }
    }

    /**
     * Klasa panelu z najwyzszymi wynikami
     */
    private class WynikiPanel extends JPanel
    {
        private class Pair
        {
            private String name;
            private int points;

            public Pair(String name, int points)
            {
                this.name=name;
                this.points=points;
            }

            public String getName() {
                return name;
            }
            public int getPoints() {
                return points;
            }
        }

        /**
         * lista z najwyzszymi wynikami
         */
        ArrayList<Pair> highscoreList;
        /**
         * Socket servwera
         */
        private Socket serverSocket;
        /**
         * zmienna okreslajaca czy gra jest w trybie online czy offline
         */
        private boolean online;


        /**
         * Konstruktor
         * @param panelWidth szerokość panelu
         * @param panelHeight wysokość panelu
         * @param menuListener listener menu głowengo
         * @param online czy gra jest online
         * @param serverSocket socket servera
         */
        public WynikiPanel(int panelWidth, int panelHeight, ActionListener menuListener, boolean online, Socket serverSocket) {
            this.online=online;
            this.serverSocket=serverSocket;
            setLayout(new BorderLayout());
            setPreferredSize(new Dimension(panelWidth, panelHeight));
            highscoreList = new ArrayList<>();

            if(online)
            {
                getHighScoresFromServer(serverSocket);
            }
            else
            {
                loadFromFile();
            }

            add(makeHighScoreTable(), BorderLayout.CENTER);
            add(makeHighScoreLabel(), BorderLayout.NORTH);
            add(makeBackToMenuButton(menuListener), BorderLayout.SOUTH);
            setVisible(true);
        }

        /**
         * Konstruktor
         * @@param online czy gra jest online
         * @param serverSocket socket servera
         */
        public WynikiPanel(Socket serverSocket, boolean online){
            this.serverSocket=serverSocket;
            this.online=online;
            highscoreList = new ArrayList<>();
            if(online)
            {
                getHighScoresFromServer(serverSocket);
            }
            else
            {
                loadFromFile();
            }
        }

        /**
         * Pobranie listy wyników z serwera
         * @param serverSocket socket serwera
         */
        private void getHighScoresFromServer(Socket serverSocket){
            try
            {
                serverSocket.getInputStream().skip(serverSocket.getInputStream().available());
                OutputStream os = serverSocket.getOutputStream();
                PrintWriter pw = new PrintWriter(os, true);
                pw.println("Pobierz_Najlepsze_Wyniki");
                InputStream is = serverSocket.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String settings = br.readLine();

                if(settings!="Niepoprawna_Komenda")
                {
                    try
                    {
                        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                        Document doc = dBuilder.parse(new InputSource(new StringReader(settings)));
                        doc.getDocumentElement().normalize();

                        NodeList nList = doc.getElementsByTagName("ScoreID");
                        for (int temp = 0; temp < nList.getLength(); temp++) {
                            Node nNode = nList.item(temp);
                            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                                Element eElement = (Element) nNode;
                                highscoreList.add(new Pair(eElement.getElementsByTagName("name").item(0).getTextContent(), Integer.parseInt(eElement.getElementsByTagName("score").item(0).getTextContent())));
                            }
                        }
                    }
                    catch (FileNotFoundException e)
                    {
                        System.out.println(e.toString());
                        JOptionPane.showMessageDialog(this, "Nie znaleziono pliku", "Nie znaleziono pliku", JOptionPane.ERROR_MESSAGE);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            catch (IOException e)
            {
                System.out.println("PLik nie mógł zostać pobrany z serwera");
                System.out.println(e);
            }
        }

        /**
         * Parser wyników z pliku
         */
        private void loadFromFile() {
            try
            {
                    File xmlInputFile = new File(Stale.xmlPlikNajlepWyn);

                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(xmlInputFile);
                    doc.getDocumentElement().normalize();

                    NodeList nList = doc.getElementsByTagName("ScoreID");
                    for (int temp = 0; temp < nList.getLength(); temp++) {
                        Node nNode = nList.item(temp);
                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element eElement = (Element) nNode;
                            highscoreList.add(new Pair(eElement.getElementsByTagName("name").item(0).getTextContent(), Integer.parseInt(eElement.getElementsByTagName("score").item(0).getTextContent())));
                        }
                    }

            }
            catch (FileNotFoundException e)
            {
                System.out.println(e.toString());
                JOptionPane.showMessageDialog(this, "Nie znaleziono pliku", "Nie znaleziono pliku", JOptionPane.ERROR_MESSAGE);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        /**
         * Zapisanie listy wyników
         */
        private void saveHighscores(){
                try {
                    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
                    Document doc = docBuilder.newDocument();
                    Element rootElement = doc.createElement("highscores");
                    doc.appendChild(rootElement);
                    for (int i = 0; i < 10; i++) {
                        if (highscoreList.size() <= i)
                            break;
                        Element ScoreID = doc.createElement("ScoreID");
                        rootElement.appendChild(ScoreID);
                        ScoreID.setAttribute("id", Integer.toString(i));
                        Element name = doc.createElement("name");
                        name.appendChild(doc.createTextNode(highscoreList.get(i).getName()));
                        ScoreID.appendChild(name);
                        Element score = doc.createElement("score");
                        score.appendChild(doc.createTextNode(Integer.toString(highscoreList.get(i).getPoints())));
                        ScoreID.appendChild(score);
                    }
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer();
                    DOMSource source = new DOMSource(doc);
                    StreamResult result = new StreamResult(new File(Stale.xmlPlikNajlepWyn));
                    transformer.transform(source, result);
                } catch (Exception e) {
                }


        }


        /**
         * generator tabeli wyników
         * @return tabela wyników
         */
        private JTable makeHighScoreTable() {
            sortList();
           // saveHighscoresToFile();
            Vector<Vector> rowData = new Vector<>();
            for (int i = 0; i < 10; i++) {
                Vector<String> row = new Vector<>();
                String rowNumber = Integer.toString(i + 1);
                row.add(rowNumber);
                row.add(highscoreList.get(i).getName());
                row.add(Integer.toString(highscoreList.get(i).getPoints()));
                rowData.add(row);
            }
            Vector<String> colLabels = new Vector<>();
            colLabels.addElement("-");
            colLabels.addElement("Nick");
            colLabels.addElement("Wynik");

            JTable highScoreLabel = new JTable(rowData, colLabels);
            highScoreLabel.setEnabled(false);
            highScoreLabel.getTableHeader().setReorderingAllowed(false);

            return highScoreLabel;
        }

        /**
         * Generator przycisku powrotu do menu
         * @param menuListener listener menu głownego
         * @return przycisk powrotu do menu
         */
        private JButton makeBackToMenuButton(ActionListener menuListener) {
            JButton backToMainMenuBtn = new JButton("Wroc do menu glownego");
            backToMainMenuBtn.setFocusable(false);
            backToMainMenuBtn.addActionListener(menuListener);
            backToMainMenuBtn.setActionCommand("BackToMenuFromScorePanel");
            return backToMainMenuBtn;
        }

        /**
         * Generator etykiety panelu wyników
         * @return etykieta penelu
         */
        private JLabel makeHighScoreLabel() {
            JLabel highScoreLbl = new JLabel("<html><br>"+"Najlepsze wyniki"+"<br><br></html>");
            highScoreLbl.setHorizontalAlignment(JLabel.CENTER);
            highScoreLbl.setVerticalAlignment(JLabel.CENTER);
            highScoreLbl.setFont(new Font("Arial", Font.PLAIN, 15));
            return highScoreLbl;
        }

        /**
         * Sortowanie listy wyników
         */
        private void sortList() {
            Collections.sort(highscoreList, new Comparator<Pair>() {
                @Override
                public int compare(Pair pair, Pair t1) {
                    if(pair.getPoints()<t1.getPoints()){
                        return 1;
                    }
                    if(pair.getPoints()>t1.getPoints())
                    {
                        return -1;
                    }
                    else
                        return 0;
                }
            });
        }

        /**
         * Pobranie najlepszego wyniku
         * @return liczba punktów
         */
        public int getHighScore(){
            return highscoreList.get(0).getPoints();
        }

        /**
         * Dodanie wyniku do listy
         * @param Name nick gracza
         * @param Points liczba punktow
         */
        private void addScore(String Name, int Points){
            if(online)
            {
                try
                {
                    serverSocket.getInputStream().skip(serverSocket.getInputStream().available());
                    OutputStream os = serverSocket.getOutputStream();
                    PrintWriter pw = new PrintWriter(os, true);
                    pw.println("Wynik_Gry:"+Name+"@"+Points);
                    getHighScoresFromServer(serverSocket);
                }
                catch (IOException e)
                {
                    System.out.println("PLik nie mógł zostać pobrany z serwera");
                    System.out.println(e);
                }
            }
            else
            {
                highscoreList.add(new Pair(Name, Points));
                sortList();
                saveHighscores();
            }
        }

    }

}
