package KlasyGry;

import Eventy.KolizjaEvent;
import Eventy.PanelInfoEvent;
import Eventy.PoleGryEvent;
import Interfejsy.InterfKolizjiObserver;
import Interfejsy.InterfPaneluInfoObserver;
import Interfejsy.InterfPaneluInfoSubject;
import Interfejsy.InterfPolaGryObserver;
import Stale.Stale;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Klasa opisująca panel informacyjny gry
 */

public class PanelInfo extends JPanel implements ActionListener, InterfKolizjiObserver, InterfPaneluInfoSubject, InterfPolaGryObserver {
    /**
     * rozmiar palenlu
     */
    private Dimension panelSize;
    /**
     * zarzadca rozkladu elementow
     */
    private GridBagConstraints gbc;
    /**
     * wysokosc panelu
     */
    private int panelHeight;
    /**
     * szsrokosc panelu
     */
    private int panelWidth;
    /**
     * layout rozkladu elementow
     */
    private GridBagLayout GBL = new GridBagLayout();
    /**
     * listener menu glownego
     */
    private ActionListener mainListener;
    /**
     * czas pozostaly do konca gry
     */
    private int timeRemaining;
    /**
     * etykieta odliczajaca czas
     */
    JLabel timeLbl1;
    /**
     * liczba pzosalych zyc
     */
    private int lifesRemaining;
    /**
     * etykieta pozostalych zyc
     */
    JLabel lifesRemainingLbl;
    /**
     * etykieta liczby punktow
     */
    JLabel pointsLbl1;
    /**
     * liczba punktów
     */
    private int amountOfPoints;
    /**
     * Tablica do przechowywania obiektów które mają być informowane po zajściu zdarzenia związanego z polem gry
     */
    private ArrayList<InterfPaneluInfoObserver> observers;
    /**
     * komenda akcji
     */
    String actionCommand;
    /**
     * Etykieta najlepszego wyniku
     */
    JLabel highScoreLbl;

    int highScore;

    /**
     * Przycisk pasuzy
     */
    JButton pause;

    JPanel pan1 = new JPanel();
    JPanel pan2 = new JPanel();
    JPanel pan3 = new JPanel();
    JPanel pan4 = new JPanel();
    JPanel pan5 = new JPanel();

   // public JLabel liveLabel;
    private JLabel timeLabel;
    private JLabel scoreLabel;
    private JLabel levelLabel;
    private int levelNum;

    public int wspPunktow;

    private int pozostPkt;

    /**
     * Konstruktor klasy PanelInfo
     * @param screenHeight wysokość okna gry
     * @param screenWidth szerokość okna gry
     * @param color kolor panelu
     * @param main Listener menu głownego
     * @param HighScore najwyzszy wynik
     */

    public PanelInfo(int screenHeight, int screenWidth, Color color, ActionListener main, int HighScore){

        observers=new ArrayList<>();
        highScore = HighScore;
        mainListener=main;
        panelHeight=500;
        panelWidth=200;
        panelSize= new Dimension(panelWidth,panelHeight);
        setPreferredSize(panelSize);
        setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));

        //mainListener=gerMainActionListener();
        levelNum=PoleGry.getLvlNum();
        wspPunktow=40;
        lifesRemaining= Stale.initLiczbeZyc;
        timeRemaining= 40;// Stale.initialTimeReaining;
        amountOfPoints =0;// Stale.initLiczbePkt;
        initializeButtonAndLabels();
        pozostPkt=timeRemaining*wspPunktow;


}



    public void setOknGlwActListner(ActionListener main){mainListener = main;}
    /**
     * Metoda pobierająca ilość punktów
     * @return ilość punktów
     */

    public int getAmountOfPoints(){
        return amountOfPoints;
    }

    /**
     * metoda inicializujaca przyciski i etykiety
     */
    private void initializeButtonAndLabels(){

        this.setBackground(new Color(183,243, 151));


        pan1.setPreferredSize(new Dimension(200,100));
        pan2.setPreferredSize(new Dimension(200,100));
        pan3.setPreferredSize(new Dimension(200,100));
        pan4.setPreferredSize(new Dimension(200,100));
        pan5.setPreferredSize(new Dimension(200,100));

        pan1.setMaximumSize(new Dimension(200,1000));
        pan2.setMaximumSize(new Dimension(200,1000));
        pan3.setMaximumSize(new Dimension(200,1000));
        pan4.setMaximumSize(new Dimension(200,1000));
        pan5.setMaximumSize(new Dimension(200,1000));

        pan1.setBackground(new Color(183,243, 151));
        pan2.setBackground(new Color(183,243, 151));
        pan3.setBackground(new Color(183,243, 151));
        pan4.setBackground(new Color(183,243, 151));
        pan5.setBackground(new Color(183,243, 151));


        pan1.setLayout(new BoxLayout(pan1,BoxLayout.LINE_AXIS));
        //.setLayout(new BoxLayout(pan2,BoxLayout.Y_AXIS));
        pan3.setLayout(new BoxLayout(pan3,BoxLayout.LINE_AXIS));
        pan4.setLayout(new BoxLayout(pan4,BoxLayout.X_AXIS));
        pan5.setLayout(new BoxLayout(pan5,BoxLayout.Y_AXIS));

        levelLabel = new JLabel("");levelLabel.setBackground(new Color(183,243, 151));
        scoreLabel = new JLabel("");
        scoreLabel.setBackground(new Color(183,243, 151));
        timeLabel = new JLabel("");
        timeLabel.setBackground(new Color(183,243, 151));
        //liveLabel = new JLabel("");liveLabel.setBackground(new Color(183,243, 151));

        pause = new JButton("Pause");
        pause.setActionCommand("PauseButton");
        pause.addActionListener(mainListener);

        pan1.add(levelLabel);
        pan1.add(scoreLabel);
        pan2.add(timeLabel);
        //pan3.add(liveLabel);

        pan5.add(pause);

        //paintLifeLevel(pan4);

        updateLives();

        refreshLables();

        this.add(pan1);
        this.add(pan2);
        this.add(pan3);
        this.add(pan4);
        this.add(pan5);
    }

    /**
     * Rysuje serduszka
     * @param pan4
     */
    private void paintLifeLevel(JPanel pan4) {
               String pathLifeIcon = "src\\Obrazki\\img1.png";
        try {
            File file = new File(pathLifeIcon);
            BufferedImage bi = ImageIO.read(file);
            ImageIcon imgIcon = new ImageIcon(bi);

            JLabel labelFoto1 = new JLabel();
            JLabel labelFoto2 = new JLabel();
            JLabel labelFoto3 = new JLabel();
            JLabel labelFoto4 = new JLabel();
            JLabel labelFoto5 = new JLabel();

            labelFoto1.setIcon(imgIcon);
            labelFoto2.setIcon(imgIcon);
            labelFoto3.setIcon(imgIcon);
            labelFoto4.setIcon(imgIcon);
            labelFoto5.setIcon(imgIcon);

            if(lifesRemaining >= 3) {
                pan4.add(labelFoto1, BorderLayout.CENTER);
                pan4.add(labelFoto2, BorderLayout.CENTER);
                pan4.add(labelFoto3, BorderLayout.CENTER);
            }
            if(lifesRemaining==2)
            {
                pan4.add(labelFoto1, BorderLayout.CENTER);
                pan4.add(labelFoto2, BorderLayout.CENTER);
            }
            if(lifesRemaining==1)
            {
                pan4.add(labelFoto1, BorderLayout.CENTER);
            }
        }
        catch (IOException ex) {
        }


    }

    /**
     * Metoda zmieniająca rozmiary panelu gdy zmieniany jest rozmiar okna gry
     * @param screenWidth szerokość okna gry
     * @param screenHeight wysokość okna gry
     */
    public void autoResize(int screenWidth, int screenHeight) {
       // panelWidth=screenWidth*(200/678);
        //panelHeight=screenHeight;
        //panelSize = new Dimension(panelWidth,panelHeight);
        //setPreferredSize(panelSize);
    }

    /**
     * Metoda odejmująca życia po wypadnięciu piłki za planszę
     */
    private void lifeLost(){
        lifesRemaining--;
        //lifesRemainingLbl.setText(Stale.napisZycia + lifesRemaining);
        updateLives();
        System.out.println("liczba zyc "+lifesRemaining);

        if(lifesRemaining<=0){
            actionCommand="LostAllLifes";
            notifyObserver();
        }
    }// Metoda nie wiem czy potrzebna

    public void updateLives(){
        String pathLifeIcon = "src\\Obrazki\\img1.png";
        try {
            File file = new File(pathLifeIcon);
            BufferedImage bi = ImageIO.read(file);
            ImageIcon imgIcon = new ImageIcon(bi);

            Component[] comp = pan4.getComponents();

            JLabel labelFoto1 = new JLabel();
            JLabel labelFoto2 = new JLabel();
            JLabel labelFoto3 = new JLabel();
            JLabel labelFoto4 = new JLabel();
            JLabel labelFoto5 = new JLabel();





            labelFoto1.setIcon(imgIcon);
            labelFoto2.setIcon(imgIcon);
            labelFoto3.setIcon(imgIcon);
            labelFoto4.setIcon(imgIcon);
            labelFoto5.setIcon(imgIcon);

            if(lifesRemaining == 5) {

                pan4.add(labelFoto1, BorderLayout.CENTER);
                pan4.add(labelFoto2, BorderLayout.CENTER);
                pan4.add(labelFoto3, BorderLayout.CENTER);
                pan4.add(labelFoto4, BorderLayout.CENTER);
                pan4.add(labelFoto5, BorderLayout.CENTER);
            }
            if(lifesRemaining == 4) {

                pan4.remove(comp[4]);

                pan4.remove(comp[3]);
                pan4.remove(comp[2]);
                pan4.remove(comp[1]);
                pan4.remove(comp[0]);

                pan4.add(labelFoto1, BorderLayout.CENTER);
                pan4.add(labelFoto2, BorderLayout.CENTER);
                pan4.add(labelFoto3, BorderLayout.CENTER);
                pan4.add(labelFoto4, BorderLayout.CENTER);
            }

            if(lifesRemaining == 3) {


/*
                pan4.remove(comp[3]);
                pan4.remove(comp[4]);
*/

                pan4.remove(comp[3]);
                pan4.remove(comp[2]);
                pan4.remove(comp[1]);
                pan4.remove(comp[0]);

                pan4.add(labelFoto1, BorderLayout.CENTER);
                pan4.add(labelFoto2, BorderLayout.CENTER);
                pan4.add(labelFoto3, BorderLayout.CENTER);
            }
            if(lifesRemaining==2)
            {
/*
                pan4.remove(comp[4]);
                pan4.remove(comp[3]);
                pan4.remove(comp[2]);
*/


                pan4.remove(comp[2]);
                pan4.remove(comp[1]);
                pan4.remove(comp[0]);

                pan4.add(labelFoto1, BorderLayout.CENTER);
                pan4.add(labelFoto2, BorderLayout.CENTER);
            }
            if(lifesRemaining==1)
            {
/*
                pan4.remove(comp[4]);
                pan4.remove(comp[3]);
                pan4.remove(comp[2]);
                pan4.remove(comp[1]);
*/


                pan4.remove(comp[1]);
                pan4.remove(comp[0]);

                pan4.add(labelFoto1, BorderLayout.CENTER);
            }
        }
        catch (IOException ex) {
        }

    }

    /**
     *
     * @param numberOfPoints liczba punktów
     */
    private void addPoints(int numberOfPoints){
        amountOfPoints+=numberOfPoints;
/*        pointsLbl1.setText(Stale.pointLabelText + amountOfPoints);
        if(amountOfPoints>highScore) {

            highScoreLbl.setText(Stale.highScoreLabelText + amountOfPoints);
            highScoreLbl.setForeground(Color.red);
        }*/
    }

    /**
     * Metoda dodajaca zycia
     */
    private void addLife(){
        if(lifesRemaining<4)
            lifesRemaining++;
        //lifesRemainingLbl.setText(Stale.napisZycia + lifesRemaining);
        updateLives();
    }

    /**
     * Metoda obsługująca zdarzenia generowane przez timer
     * @param actionEvent zdarzenie Timera
     */
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String actCommand = actionEvent.getActionCommand();
        if(actCommand=="TimerTic")
        {
            timeRemaining--;
            System.out.println("PozostalyCzas " +timeRemaining);
           // timeLbl1.setText("Pozostały czas: " + timeRemaining);
            refreshLables();
          //  System.out.println("PKT"+ amountOfPoints);
           // System.out.println("PozostPKT"+ amountOfPoints);
            if(timeRemaining<=0)
            {
                actionCommand="TimeOver";
                notifyObserver();
            }

        }
    }
    private void refreshLables() {
        timeLabel.setText("           Pozostalo czasu:"+ timeRemaining );
        levelNum=PoleGry.getLvlNum();
        levelLabel.setText("           Poziom:" + levelNum );
        //addPoints(wspPunktow);
        pozostPkt-=wspPunktow;
        scoreLabel.setText("    Punkty: " + (amountOfPoints+pozostPkt));
    }
    /**
     * metoda oblsugujaca zdarzenie kolizji
     * @param ActionEvent Obiekk eventu kolizji
     */
    @Override
    public void collisionActionPerformed(KolizjaEvent ActionEvent)
    {
        String actionCommand=ActionEvent.getActionCommand();
        switch (actionCommand){
            case "BallOutOfGameScreen":
                lifeLost();
               // notifyObserver();
                //System.out.println("liczba zyc "+lifesRemaining);
                break;
            case "EndLevelCollision":
                //lifeLost();
                //lifesRemaining=lifesRemaining-1;
               // notifyObserver();
                //System.out.println("liczba zyc "+lifesRemaining);
                break;
            case "BramkaColision":
                lifeLost();
                //System.out.println("liczba zyc "+lifesRemaining);
                break;
            case "PrzelacznikColision":
                break;
            case "PrzeszkodaCollision":
                lifeLost();
                //System.out.println("liczba zyc "+lifesRemaining);
                break;
            default:
                break;
        }
    }

    /**
     * rejestracja listenerow
     * @param observer listener
     */
    @Override
    public void register(InterfPaneluInfoObserver observer) {
        observers.add(observer);
    }

    /**
     * usuwanie listenerow
     * @param observer listener
     */
    @Override
    public void unregister(InterfPaneluInfoObserver observer) {
        observers.remove(observers.indexOf(observer));
    }

    /**
     * informowanie listenerow ze zaszlo zdarzenie
     */
    @Override
    public void notifyObserver() {
        boolean timeOver=false;
        boolean dead=false;
        if(timeRemaining<=0)
        {
            timeOver=true;
        }
        if(lifesRemaining<=0)
        {
            dead=true;
        }
        PanelInfoEvent ActionEvent=new PanelInfoEvent(actionCommand, amountOfPoints);
        for(InterfPaneluInfoObserver observer : observers){
            observer.infoPanelActionPerformed(ActionEvent);
        }
    }

    /**
     * Metoda rozwiązująca zajście zdarzenia z planszą gry
     * @param ActionEvent obiekt zdarzenia planszy gry
     */
    @Override
    public void poleGryActionPerformed(PoleGryEvent ActionEvent) {
        String cmd=ActionEvent.getActionCommand();
        switch (cmd){
            case "LevelLoaded":
                timeRemaining=ActionEvent.getLvlTime();
                wspPunktow=ActionEvent.getWspOcen();
                pozostPkt=wspPunktow*timeRemaining;
                break;
            case "NoMoreLevelsAvailable":
                //addPoints(timeRemaining*40);
                //addPoints(pozostPkt);
                actionCommand="NoMoreLevelsAvailable";
                notifyObserver();
                break;
            case "LoadingNewLevel":

                addPoints(timeRemaining*wspPunktow);
                wspPunktow=0;

                break;
            default:
                break;
        }
    }

}
