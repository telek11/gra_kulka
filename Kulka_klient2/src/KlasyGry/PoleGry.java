package KlasyGry;


import Eventy.PoleGryEvent;
import Stale.Stale;
import Eventy.KolizjaEvent;
import Interfejsy.InterfKolizjiObserver;
import Interfejsy.InterfPolaGryObserver;
import Interfejsy.InterfPolaGrySubject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.io.*;




/**
 * Klasa panelu gry
 */
public class PoleGry extends JPanel implements KeyListener, Runnable, InterfPolaGrySubject, InterfKolizjiObserver {


    /**
     * Parametry konca levelu
     */
    public EndLevelPlace endLevel;
    /**
     * Zmienna reprezentujaca przelacznik
     */
     public Przelacznik przelacznik;
    /**
     * zmienna reprezentujaca bramke
     */
    public Bramka bramka;

    /////////////////////////////////////////////////////
    /**
    * parametry kulki
    */
    public static int kulkax;
    public static int kulkay;
    public static int kulkar;
    /**
     * parametry konca etapu
     */
    public static int koniecx;
    public static int koniecy;
    public static int koniecwidth;
    /**
     * parametry przelacznika
     */
    public static int przelaczx;
    public static int przelaczy;
    public static int przelaczwidth;
    /**
     * parametry przeszkody 1
     */
    public static int bramkax;
    public static int bramkay;
    public static int bramkawidth;
    public static int bramkaheight;
    /**
     * parametry przeszkody 1
     */
    public static int prz1x;
    public static int prz1y;
    public static int prz1width;
    public static int prz1height;
    /**
     * parametry przeszkody 2
     */
    public static int prz2x;
    public static int prz2y;
    public static int prz2width;
    public static int prz2height;
    /**
     * parametry przeszkody 3
     */
    public static int prz3x;
    public static int prz3y;
    public static int prz3width;
    public static int prz3height;
    /**
     * parametry przeszkody 4
     */
    public static int prz4x;
    public static int prz4y;
    public static int prz4width;
    public static int prz4height;
    /**
     * parametry przeszkody 5
     */
    public static int prz5x;
    public static int prz5y;
    public static int prz5width;
    public static int prz5height;

    /**
     * Czas na pozion
     */
    private static int czasNaPoziom;
    /**
     * Wspolczynnik oceniania
     */
    private static int wspOceniania;
    ///////////////////////////////////////

    /**
     * Tablica przeszkod     *
     */
    public Przeszkoda[] arrPrzeszkod;
    /**
     * Zmienna określająca wysokość panelu gry
     */
    private int panelHeight;
    /**
     * Zmienna określająca szerokość panelu gry
     */
    private int panelWidth;
    /**
     * Zmienna określająca wymiar okna
     */
    private Dimension gameScreenSize;
    /**
     * zmienna opisujaca ilosc FPS
     */
    private int FPS;
    /**
     * Obiekt reprezentujący piłkę
     */
    private Kulka kulka;
    /**
     * Obiekt obsługujący kolizje
     */
    private Kolizja CE;
    /**
     * Zmienna określająca czy gra jest w stanie pauzy czy nie
     */
    private boolean gamePaused;
    /**
     * Zmienna określająca czy gra się skończyła
     */
    private boolean gameOver;
    /**
     *  Lista obserwatorów planszy gry
     */
    private ArrayList<InterfPolaGryObserver> observers;
    /**
     * Czas na poziom
     */
    private int levelTime;
    /**
     * Zmienna pomocnicza czy wczytywany jest nowy poziom
     */
    private boolean nextLevel;
    /**
     * Zmienna określająca aktualny poziom gry
     */
    private static int level;
    /**
     * Zmienna określająca numer pierwszego poziomu
     */
    private int firstLevel;
    /**
     * Zmienna określająca numer ostatniego poziomu
     */
    private int lastLevel;
    /**
     * Socket servera
     */
    private Socket serverSocket;



    /**
     * Kontruktor panelu gry
     * @param screenWidth szerokość panelu gry
     * @param screenHeight wysokość paneli gry
     * @param serverSocket socket servera
     */
    public PoleGry(int screenWidth, int screenHeight, Socket serverSocket)
    {
        this.serverSocket = serverSocket;
        observers = new ArrayList<>();

        gamePaused=true;
        panelHeight = 500;//(int)Math.round(screenHeight*Stale.gameScreenPanelHeightRatio);
        panelWidth = 478;//(int) (Stale.gameScreenPanelWidthRatio*screenWidth);
        gameScreenSize = new Dimension(panelWidth, panelHeight);
        setPreferredSize(gameScreenSize);
        FPS = Stale.FPS;
        addComponents();
        this.setFocusable(true);
        addKeyListener(this);
        gamePaused=true;
        gameOver=false;
    }

    /**
     * Metoda służąca do zatrzymania gry
     */
    public void pause()
    {
        if(gamePaused)
        {
            gamePaused=false;
        }
        else if(!gamePaused)
        {
            gamePaused=true;
        }
    }

    /**
     * metoda koncza gre
     */
    public void gameOver()
    {
        gameOver=true;
    }

    /**
     * dodawanie listenerow do kolizji
     * @param panelInfo panel informacyjny
     * @param oknoGlow glowne okno gry
     */
    public void addListeners(PanelInfo panelInfo, OknoGlow oknoGlow)
    {
        CE.register(kulka);
        CE.register(this);
        CE.register(panelInfo);
        CE.register(oknoGlow);
    }

    /**
     * Metoda odpowidzialna za dsotowanie panelu gry do aktualnego rozmiaru okna
     * @param screenWidth szerokość panelu
     * @param screenHeight wysokość panelu
     */
    public void autoResize(int screenWidth,int screenHeight) {
        panelHeight = screenHeight;
        panelWidth = (478/678)*screenWidth;
    }

    /**
     * Metoda wczytująca pierwszy poziom
     */
    private void loadFirstLevel(){

        if(serverSocket!=null) {
            try {
                //zniszczenie czegokolwiek co zostalo w buforze
                serverSocket.getInputStream().skip(serverSocket.getInputStream().available());
                OutputStream os = serverSocket.getOutputStream();
                os.flush();
                PrintWriter pw = new PrintWriter(os, true);
                pw.flush();
                pw.println("Pobierz_dostepne_poziomy");
                InputStream is = serverSocket.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String availbleLevels = br.readLine();
                if (availbleLevels != "Niepoprawna_komenda")
                {
                    String str[] = availbleLevels.split(" ");
                    firstLevel = Integer.parseInt(str[2].split(";")[0]);
                    lastLevel = Integer.parseInt(str[4]);
                    level = firstLevel;
                    pw.println("Pobierz_Poziom:" + firstLevel);
                    is = serverSocket.getInputStream();
                    br = new BufferedReader(new InputStreamReader(is));
                    String firstLevelXml = br.readLine();

				   System.out.println(firstLevelXml);

					parsowanie(firstLevelXml);
                }
            } catch (IOException e) {
                System.out.println("PLik nie mógł zostać pobrany z serwera");
                System.out.println(e);
            }
        }
        else{

            firstLevel= Stale.numerPierwPoziom;
            lastLevel= Stale.numerOstatnPoziom;
            level=firstLevel;
            parsowanie(level);
        }

        initPrzeszArr();



        kulka = new Kulka(panelWidth, panelHeight, FPS);
        kulka.changeStartPos(kulkax,kulkay);
        add(kulka);

        endLevel = new EndLevelPlace(koniecx,koniecy,koniecwidth);
        add(endLevel);

        przelacznik = new Przelacznik(przelaczx,przelaczy,przelaczwidth);
        add(przelacznik);

        bramka = new Bramka (bramkax,bramkay,bramkawidth,bramkaheight);
        add(bramka);

        levelTime=czasNaPoziom;
        notifyObserver(new PoleGryEvent("LevelLoaded", levelTime,wspOceniania));
        CE=new Kolizja(kulka,arrPrzeszkod,endLevel,przelacznik,bramka);

        gamePaused = false;
        gameOver = false;
        nextLevel = false;

/*        if(gamePaused)
            System.out.println("wwczytano 1 level game pasued: true");
        else
            System.out.println("wwczytano 1 level gry game pasued: false");*/
    }

    /**
     * Metoda wczytująca kolejny poziom
     */
    private void loadNextLevel(){
        level++;
        notifyObserver(new PoleGryEvent("LoadingNewLevel",0,0));

        if(level<=lastLevel) {
            remove(endLevel);
            remove(arrPrzeszkod[0]);
            remove(arrPrzeszkod[1]);
            remove(arrPrzeszkod[2]);
            remove(arrPrzeszkod[3]);
            remove(arrPrzeszkod[4]);
            remove(przelacznik);
            remove(kulka);

                if(serverSocket!=null)
				{
					try {
						serverSocket.getInputStream().skip(serverSocket.getInputStream().available());
						OutputStream os = serverSocket.getOutputStream();
						PrintWriter pw = new PrintWriter(os, true);
                        pw.println("Pobierz_Poziom:" + level);

                        InputStream is = serverSocket.getInputStream();
						BufferedReader br = new BufferedReader(new InputStreamReader(is));
                        String levelXml = br.readLine();

                        if (levelXml != "Niepoprawna_komenda")
                        {
							parsowanie(levelXml);
						}
					}
					catch (IOException e) {
						System.out.println("PLik nie mógł zostać pobrany z serwera");
						System.out.println(e);
					}
				}
				else
                {
                    parsowanie(level);
                }

            initPrzeszArr();

            kulka.changeStartPos(kulkax,kulkay);
            add(kulka);

            endLevel = new EndLevelPlace(koniecx,koniecy,koniecwidth);
            add(endLevel);

            przelacznik = new Przelacznik(przelaczx,przelaczy,przelaczwidth);
            add(przelacznik);

            bramka = new Bramka (bramkax,bramkay,bramkawidth,bramkaheight);
            add(bramka);

            levelTime=czasNaPoziom;
            notifyObserver(new PoleGryEvent("LevelLoaded", levelTime,wspOceniania));
            CE.setNewPar(arrPrzeszkod, kulka,endLevel,przelacznik,bramka);

            gamePaused = false;
            gameOver = false;
            nextLevel = false;


        }
        else
            notifyObserver(new PoleGryEvent("NoMoreLevelsAvailable", 0,0));
    }


    /**
     * Getter numeru poziomu
     * @return numer poziomu
     */
    public static int getLvlNum(){
        int lvlNum=level;

        return lvlNum;
    }

    /**
     * Metoda służąca do tworzenia tablicy przeszkod
     */
    private void initPrzeszArr()
    {
        arrPrzeszkod = new Przeszkoda[5];
        arrPrzeszkod[0]= new Przeszkoda(prz1x,prz1y,prz1width,prz1height);
        arrPrzeszkod[1]= new Przeszkoda(prz2x,prz2y,prz2width,prz2height);
        arrPrzeszkod[2]= new Przeszkoda(prz3x,prz3y,prz3width,prz3height);
        arrPrzeszkod[3]= new Przeszkoda(prz4x,prz4y,prz4width,prz4height);
        arrPrzeszkod[4]= new Przeszkoda(prz5x,prz5y,prz5width,prz5height);

        add(arrPrzeszkod[0]);
        add(arrPrzeszkod[1]);
        add(arrPrzeszkod[2]);
        add(arrPrzeszkod[3]);
        add(arrPrzeszkod[4]);
    }

    /**
     * Metoda tworząca przeszkody w o odpowiednich rozmiarach i odpowiednim miejscu
     */
    private void drawPrzesz(Graphics g){
        arrPrzeszkod[0].paintComponent(g);
        arrPrzeszkod[1].paintComponent(g);
        arrPrzeszkod[2].paintComponent(g);
        arrPrzeszkod[3].paintComponent(g);
        arrPrzeszkod[4].paintComponent(g);
    }

    /**
     * Metoda inicjująca obiekty
     */
    private void addComponents()
    {
        loadFirstLevel();
    }

    /**
     * Metoda służąca do rysownia pola gry
     * @param g grafika
     */
    public void paint(Graphics g) {
        draw(g);
    }

    /**
     * Metoda opisująca w jaki sposób mają być malowane odpowienie komponenty
     * @param g grafika
     */
    private void draw(Graphics g)
    {
        if(!nextLevel) {
            drawPrzesz(g);
            drawEndLevel(g);
            drawBall(g);
            drawPrzelacznik(g);
            drawBramka(g);
        }
    }

    /**
     * Metoda służąca do rysownia koncz poziomu
     * @param g grafika
     */
    private void drawEndLevel(Graphics g)
    {
        endLevel.paintComponent(g);
    }
    /**
     * Metoda służąca do rysownia przelacznika
     * @param g grafika
     */
    private void drawPrzelacznik(Graphics g)
    {
        przelacznik.paintComponent(g);
    }
    /**
     * Metoda służąca do rysownia bramki
     * @param g grafika
     */
    private void drawBramka(Graphics g) {bramka.paintComponent(g);}

    /**
     * Metoda opisująca jak ma być malowana piłka
     * @param g grafika
     */
    private void drawBall(Graphics g) {
        kulka.paint(g);
    }

    /**
     * Metoda opisująca co ma się stać gdy naciśnięty zostanie przycisk
     * @param keyEvent zdarzenie zwiaznae z klawiatura
     */
    @Override
    public void keyTyped(KeyEvent keyEvent) {}

    /**
     * Metoda opisująca co ma się stać gdy naciśnięty się przycisk
     * @param keyEvent zdarzenie zwiaznae z klawiatura
     */
    @Override
    public void keyPressed(KeyEvent keyEvent) {kulka.keyPressed(keyEvent);}

    /**
     * Metoda opisująca co ma się stać gdy zwolniony zostanie przycisk
     * @param keyEvent zdarzenie zwiaznae z klawiatura
     */
    @Override
    public void keyReleased(KeyEvent keyEvent) {
        kulka.keyReleased(keyEvent);
    }

    /**
     * Metoda obsługująca wątek głowny programu
     */
    @Override
    public void run() {
        try {
            while (true) {
                    //gamePaused=true;
                    System.out.print("");
                    if(!gamePaused && !gameOver && !nextLevel) {
                        long nanoStart = System.nanoTime();
                        CE.checkCollisions(getWidth(), getHeight());
                        kulka.update();
                        /* if(gamePaused)
                            System.out.println("glowny watek gamepasued=true");
                        else
                            System.out.println("glowny watek gamepasued=false");
                            */

                        long nanoEnd = System.nanoTime();
                        long nanoTime = nanoEnd - nanoStart;
                        double seconds = (double) nanoTime / 1000000.0;
                        if (seconds < Stale.czasSnuThread)
                            Thread.sleep(Stale.czasSnuThread - (long) seconds);
                    }

                    if(nextLevel){
                        loadNextLevel();
                        nextLevel=false;
                    }
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


    /**
     * Rejestracja obserwatorów ekrany gry
     * @param observer obserwator
     */
    @Override
    public void register(InterfPolaGryObserver observer) {
        observers.add(observer);
    }

    /**
     * Usuwanie obserwatora planszy gry
     * @param observer obserwator
     */
    @Override
    public void unregister(InterfPolaGryObserver observer) {
        observers.remove(observers.indexOf(observer));
    }

    /**
     * Informowanie obserwattorów że zaszło  zdarszenie związanie z planszą gry
     * @param ActionEvent obiekt zdarzenia panelu gry
     */
    @Override
    public void notifyObserver(PoleGryEvent ActionEvent) {
            for(InterfPolaGryObserver observer : observers)
            {
                observer.poleGryActionPerformed(ActionEvent);
            }
    }
    /**
     * Wersja parsowania parsujaca stringa
     * @param ciagZnakow
     */
    public static void parsowanie(String ciagZnakow)
    {
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            String wyraz=ciagZnakow;
            DocumentBuilder dBuilder = factory.newDocumentBuilder();
            Document doc = dBuilder.parse(new InputSource(new StringReader(wyraz)));

            doc.getDocumentElement().normalize();

            kulkax = Integer.parseInt(doc.getElementsByTagName("kulkax").item(0).getTextContent());
            kulkay = Integer.parseInt(doc.getElementsByTagName("kulkay").item(0).getTextContent());
            kulkar = Integer.parseInt(doc.getElementsByTagName("kulkar").item(0).getTextContent());

            koniecx = Integer.parseInt(doc.getElementsByTagName("koniecx").item(0).getTextContent());
            koniecy = Integer.parseInt(doc.getElementsByTagName("koniecy").item(0).getTextContent());
            koniecwidth = Integer.parseInt(doc.getElementsByTagName("koniecwidth").item(0).getTextContent());

            czasNaPoziom = Integer.parseInt(doc.getElementsByTagName("czasNaPoziom").item(0).getTextContent());
            wspOceniania = Integer.parseInt(doc.getElementsByTagName("wspOceniania").item(0).getTextContent());

            przelaczx = Integer.parseInt(doc.getElementsByTagName("przelaczx").item(0).getTextContent());
            przelaczy = Integer.parseInt(doc.getElementsByTagName("przelaczy").item(0).getTextContent());
            przelaczwidth = Integer.parseInt(doc.getElementsByTagName("przelaczwidth").item(0).getTextContent());

            bramkax = Integer.parseInt(doc.getElementsByTagName("bramkax").item(0).getTextContent());
            bramkay = Integer.parseInt(doc.getElementsByTagName("bramkay").item(0).getTextContent());
            bramkawidth = Integer.parseInt(doc.getElementsByTagName("bramkawidth").item(0).getTextContent());
            bramkaheight = Integer.parseInt(doc.getElementsByTagName("bramkaheight").item(0).getTextContent());

            prz1x = Integer.parseInt(doc.getElementsByTagName("prz1x").item(0).getTextContent());
            prz1y = Integer.parseInt(doc.getElementsByTagName("prz1y").item(0).getTextContent());
            prz1width = Integer.parseInt(doc.getElementsByTagName("prz1width").item(0).getTextContent());
            prz1height = Integer.parseInt(doc.getElementsByTagName("prz1height").item(0).getTextContent());

            prz2x = Integer.parseInt(doc.getElementsByTagName("prz2x").item(0).getTextContent());
            prz2y = Integer.parseInt(doc.getElementsByTagName("prz2y").item(0).getTextContent());
            prz2width = Integer.parseInt(doc.getElementsByTagName("prz2width").item(0).getTextContent());
            prz2height = Integer.parseInt(doc.getElementsByTagName("prz2height").item(0).getTextContent());

            prz3x = Integer.parseInt(doc.getElementsByTagName("prz3x").item(0).getTextContent());
            prz3y = Integer.parseInt(doc.getElementsByTagName("prz3y").item(0).getTextContent());
            prz3width = Integer.parseInt(doc.getElementsByTagName("prz3width").item(0).getTextContent());
            prz3height = Integer.parseInt(doc.getElementsByTagName("prz3height").item(0).getTextContent());

            prz4x = Integer.parseInt(doc.getElementsByTagName("prz4x").item(0).getTextContent());
            prz4y = Integer.parseInt(doc.getElementsByTagName("prz4y").item(0).getTextContent());
            prz4width = Integer.parseInt(doc.getElementsByTagName("prz4width").item(0).getTextContent());
            prz4height = Integer.parseInt(doc.getElementsByTagName("prz4height").item(0).getTextContent());

            prz5x = Integer.parseInt(doc.getElementsByTagName("prz5x").item(0).getTextContent());
            prz5y = Integer.parseInt(doc.getElementsByTagName("prz5y").item(0).getTextContent());
            prz5width = Integer.parseInt(doc.getElementsByTagName("prz5width").item(0).getTextContent());
            prz5height = Integer.parseInt(doc.getElementsByTagName("prz5height").item(0).getTextContent());

        }
        catch (ParserConfigurationException e)
        {
            e.printStackTrace();
        }
        catch (SAXException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     *  Wersja parsowania poziomu otrzymujaca jako argument numer levelu
     * @param numerLevel numer levelu
     */
    public static void parsowanie(int numerLevel)
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try
        {
            int numLevel=numerLevel;
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc= builder.parse("PlikiKonfiguracyjne\\level"+numLevel+".xml");

            doc.getDocumentElement().normalize();

            kulkax = Integer.parseInt(doc.getElementsByTagName("kulkax").item(0).getTextContent());
            kulkay = Integer.parseInt(doc.getElementsByTagName("kulkay").item(0).getTextContent());
            kulkar = Integer.parseInt(doc.getElementsByTagName("kulkar").item(0).getTextContent());

            koniecx = Integer.parseInt(doc.getElementsByTagName("koniecx").item(0).getTextContent());
            koniecy = Integer.parseInt(doc.getElementsByTagName("koniecy").item(0).getTextContent());
            koniecwidth = Integer.parseInt(doc.getElementsByTagName("koniecwidth").item(0).getTextContent());

            czasNaPoziom = Integer.parseInt(doc.getElementsByTagName("czasNaPoziom").item(0).getTextContent());
            wspOceniania = Integer.parseInt(doc.getElementsByTagName("wspOceniania").item(0).getTextContent());

            przelaczx = Integer.parseInt(doc.getElementsByTagName("przelaczx").item(0).getTextContent());
            przelaczy = Integer.parseInt(doc.getElementsByTagName("przelaczy").item(0).getTextContent());
            przelaczwidth = Integer.parseInt(doc.getElementsByTagName("przelaczwidth").item(0).getTextContent());

            bramkax = Integer.parseInt(doc.getElementsByTagName("bramkax").item(0).getTextContent());
            bramkay = Integer.parseInt(doc.getElementsByTagName("bramkay").item(0).getTextContent());
            bramkawidth = Integer.parseInt(doc.getElementsByTagName("bramkawidth").item(0).getTextContent());
            bramkaheight = Integer.parseInt(doc.getElementsByTagName("bramkaheight").item(0).getTextContent());

            prz1x = Integer.parseInt(doc.getElementsByTagName("prz1x").item(0).getTextContent());
            prz1y = Integer.parseInt(doc.getElementsByTagName("prz1y").item(0).getTextContent());
            prz1width = Integer.parseInt(doc.getElementsByTagName("prz1width").item(0).getTextContent());
            prz1height = Integer.parseInt(doc.getElementsByTagName("prz1height").item(0).getTextContent());

            prz2x = Integer.parseInt(doc.getElementsByTagName("prz2x").item(0).getTextContent());
            prz2y = Integer.parseInt(doc.getElementsByTagName("prz2y").item(0).getTextContent());
            prz2width = Integer.parseInt(doc.getElementsByTagName("prz2width").item(0).getTextContent());
            prz2height = Integer.parseInt(doc.getElementsByTagName("prz2height").item(0).getTextContent());

            prz3x = Integer.parseInt(doc.getElementsByTagName("prz3x").item(0).getTextContent());
            prz3y = Integer.parseInt(doc.getElementsByTagName("prz3y").item(0).getTextContent());
            prz3width = Integer.parseInt(doc.getElementsByTagName("prz3width").item(0).getTextContent());
            prz3height = Integer.parseInt(doc.getElementsByTagName("prz3height").item(0).getTextContent());

            prz4x = Integer.parseInt(doc.getElementsByTagName("prz4x").item(0).getTextContent());
            prz4y = Integer.parseInt(doc.getElementsByTagName("prz4y").item(0).getTextContent());
            prz4width = Integer.parseInt(doc.getElementsByTagName("prz4width").item(0).getTextContent());
            prz4height = Integer.parseInt(doc.getElementsByTagName("prz4height").item(0).getTextContent());

            prz5x = Integer.parseInt(doc.getElementsByTagName("prz5x").item(0).getTextContent());
            prz5y = Integer.parseInt(doc.getElementsByTagName("prz5y").item(0).getTextContent());
            prz5width = Integer.parseInt(doc.getElementsByTagName("prz5width").item(0).getTextContent());
            prz5height = Integer.parseInt(doc.getElementsByTagName("prz5height").item(0).getTextContent());

        }
        catch (ParserConfigurationException e)
        {
            e.printStackTrace();
        }
        catch (SAXException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Metoda obslugujaca kolizje
     * @param ActionEvent Obiekt eventu kolizji
     */
    @Override
    public void collisionActionPerformed(KolizjaEvent ActionEvent)
    {
        String actionCommand=ActionEvent.getActionCommand();
        switch (actionCommand)
        {
            case "EndLevelCollision":
				// Co ma sie zdarzyc w chwili "uderzenia" w pole koniec gry
                //pause();
                System.out.println("Koniec  Lev game screen");
                nextLevel=true;
                break;
            case "PrzeszkodaCollision":
                //pause();
				// Co ma sie zdarzyc w chwili uderzenia w preszkode
				break;
            case "BramkaColision":
                break;
			case "BallOutOfScreen":
			    //pause();
				// co ma sie zdarzyc w chwili uderzenia w krawedz ekranu
				break;
            case "PrzelacznikColision":
                bramka.fillData(0,0,0,0);
                break;


            default:
                break;
        }
    }
}
