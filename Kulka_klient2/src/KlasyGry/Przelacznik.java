package KlasyGry;


import javax.swing.*;
import java.awt.*;

/**
 * Klasa opisujaca przelacznik
 */
public class Przelacznik extends JComponent {
    /**
     * Wsp x przelacznika
     */
    public int x;
    /**
     * Wsp y przelacznika
     */
    public int y;
    /**
     * Szerokosc kwadratu reprezentujacaego przelacznik
     */
    public int width;

    /**
     * Konstruktor przelacznika
     * @param xP wsp x
     * @param yP wsp y
     * @param widhtP szerokosc
     */
    public Przelacznik(int xP, int yP, int widhtP)
    {
        fillData(xP,yP,widhtP);
    }

    /** Mtoda tworzaca kwadrat reprenzentijacy przelacznik
     * metoda
     * @param xK wsp x konca levelu
     * @param yK wsp y konca levelu
     * @param widhtK Szerokosc kwadratu reprezentujacego przelacznik
     */
    private void fillData(int xK,int yK,int widhtK)
    {
        x=xK;
        y=yK;
        width=widhtK;
    }

    /**
     * Metoda rysujaca komponent graficzny
     * @param g parametr graficzny
     */
    protected void paintComponent(Graphics g)
    {
        int widthParentScreen = this.getParent().getWidth();
        int heightParentScreen =  this.getParent().getHeight();
        g.setColor(new Color(133,120,167));
        g.fillRect(x*widthParentScreen/1000,y*heightParentScreen/1000,width*widthParentScreen/1000,width*heightParentScreen/1000);
    }

    /**
     * Metoda zwracajaca dolno krawedz
     * @return dolna krawedz
     */
    public double getDownCordinate(){ return(y+width);}

    /**
     * Metoda zwrcajaca gorna krawedz
     * @return gorna krawedz
     */
    public double getUpCordinate(){ return(y);}

    /**
     * Metoda zwracajaca lewa krawedz
     * @return lewa krawedz
     */
    public double getLeftCordinate(){ return(x);}

    /**
     * Metoda zwracajaca prawa krawedz
     * @return prawa krawedz
     */
    public double getRightCordinate(){ return(x+width);}

}
