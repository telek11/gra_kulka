package KlasyGry;


import javax.swing.*;
import java.awt.*;

/**
 * Klasa Opisujaca Przeszkode
 */
public class Przeszkoda extends JComponent {
    /**
     * Polozenie x przeszkody
     */
    public int x;
    /**
     * Polozenie Y przeszkody
     */
    public int y;
    /**
     * szerokosc przeszkody
     */
    public int width;
    /**
     * wysokosc przeszkody
     */
    public int height;

    /**
     * Konstruktor klasy przeszkoda
     * @param xK Polozenie x przeszkody
     * @param yK polozenie y przeszkody
     * @param widhtK szerokosc przeszkody
     * @param heightK wyskosc przeszkody
     */
    public Przeszkoda(int xK, int yK, int widhtK, int heightK)
    {
        fillData(xK,yK,widhtK,heightK);
    }

    /**
     *
     * @param xK
     * @param yK
     * @param widhtK
     * @param heightK
     */
    private void fillData(int xK,int yK,int widhtK,int heightK)
    {
        x=xK;
        y=yK;
        width=widhtK;
        height=heightK;
    }

    /**
     * Metoda rysujaca przeszkode
     * @param g parametr graficzny
     */
    protected void paintComponent(Graphics g)
    {
        int widthParScreen= this.getParent().getWidth();
        int heightParScreen= this.getParent().getHeight();

        g.setColor(new Color(37, 40, 170));
        g.fillRect(x*widthParScreen/1000,y*heightParScreen/1000,width*widthParScreen/1000,height*heightParScreen/1000);
    }

    /**
     * Metoda zwracajaca dolno krawedz
     * @return dolna krawedz
     */
    public double getDownCordinate(){ return(y+height);}

    /**
     * Metoda zwrcajaca gorna krawedz
     * @return gorna krawedz
     */
    public double getUpCordinate(){ return(y);}

    /**
     * Metoda zwracajaca lewa krawedz
     * @return lewa krawedz
     */
    public double getLeftCordinate(){ return(x);}

    /**
     * Metoda zwracajaca prawa krawedz
     * @return prawa krawedz
     */
    public double getRightCordinate(){ return(x+width);}
}
