package KlasyGry;

import Stale.Stale;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.Socket;


/**
 * Klasa uruchamia programm
 */
public class StartGry {
    /**
     * Obiekt okna menu glownego
     */
    public static OknoMenu oknoMenu;


    /**
     * Opisuje start programu
     * @param socket
     */
    public static void runGame(Socket socket)
    {
        Socket serverSocket = socket;
        if(serverSocket!=null) // Oznacza : Jezeli nastopilo poprawne logoewanie
        {
            getSettings(serverSocket);      // pobranie ustawien z serwera
            Stale.parseMenuConfigurationFile(); // Parsowanie pliku zawierajacego stale
        }
        else {
            Object[] options={"Tak","Nie"};

            switch(JOptionPane.showOptionDialog(null, "Nie moge polaczyc sie z serwerem. Czy chcesz grac offline?", "Brak polaczenia z serwerem", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,null,options,options[1])){
                case JOptionPane.YES_OPTION: // Wybor opcji tak nie zmienia nic wiec program kontynuje swoja prace
                    break;
                case JOptionPane.NO_OPTION: // Wybor opcji Nie powoduje zakonczenie pracy programu
                    System.exit(0);
                    break;
                default:
                    break;
            }
        }
        makeGlowMenu(serverSocket); // utworzenie menu glownego z parametrem serverSocket ktory moze cos zawierac(gra online) lub byc pusty - null (gra offline)
    }

    /**
     * Metoda do tworzenia glownego menu
     * @param serverSocket
     */
    public static void makeGlowMenu(Socket serverSocket)
    {
        oknoMenu = new OknoMenu(new Dimension(Stale.szerokOknMenuGlow, Stale.wysokOknMenuGlow),serverSocket);
        oknoMenu.setVisible(true);
    }

    /**
     * Metoda pobierajaca pliki konfiguracyjne z serwera i ich zapisanie lokalnie
     * @param serverSocket socket serwera
     */
    private static void getSettings(Socket serverSocket){

        try{
            OutputStream os = serverSocket.getOutputStream();
            PrintWriter pw = new PrintWriter(os, true);
            pw.println("Pobierz_Ustawienia"); // Stworzenie strumienia wyjsciowego i wyslanie do serwera komendy Pobierz ustawienia

            InputStream is = serverSocket.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String settings = br.readLine();
            PrintWriter out = new PrintWriter("PlikiKonfiguracyjne\\config.xml"); //odebranie od serwera ustwien i zapisanie ich w lokalnym pliku konfiguracyjnym
            out.println(settings); //out to plik konfiguracyjny
            out.close();
        }
        catch (IOException e)
        {
            System.out.println("Nastapil blad polaczenia !");
            System.out.println(e);
        }
    }
}

