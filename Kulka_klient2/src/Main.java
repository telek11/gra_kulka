import KlasyGry.StartGry;

import javax.swing.*;
import java.io.*;
import java.net.Socket;


/**
 * Glowna metoda aplikacji
 */
public class Main {

    /**
     * Adres Ip serwera
     */
    private static String IPAddress;
    /**
     * Port serwera
     */
    private static int Port;


    /**
     * Metoda main
     * @param args argumenty
     */
    public static void main(String[] args)
    {
        Object[] options = {"Online", "Offline"};

        switch (JOptionPane.showOptionDialog(null, "W jakim Trybie chcesz grac ?", "Wybór trybu gry", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[1]))
        {
            case JOptionPane.YES_OPTION: // Wybor opcji ONLINE
                StartGry.runGame(polaczZSerwerem()); //
                break;
            case JOptionPane.NO_OPTION: // wybór trybu offline
                StartGry.makeGlowMenu(null);
                break;
            default:
                break;

        }
    }

    /**
     * Metoda nawiazuje polaczenie z serwerem
     * @return Socket serwera
     */
    private static Socket polaczZSerwerem() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("PlikiKonfiguracyjne\\IPINFO.txt"));
            IPAddress=br.readLine();
            Port=Integer.parseInt(br.readLine());
            Socket serverSocket = new Socket(IPAddress, Port); //Socket ktory mowi Gdzie maja byc wysylane i zkad maja byc pobierane polecnia
            OutputStream os = serverSocket.getOutputStream();
            PrintWriter pw = new PrintWriter(os, true);
            pw.println("Zaloguj");                              // Wysyla do serwera "Zaloguj"
            InputStream is = serverSocket.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            if(br.readLine().contains("Zalogowano"))    // Jezeli odebrano "zalogowano" Zwraca socket serwera potrzebny do uruchomienia gry online
            {
                return serverSocket;
            }
            else        //Jezeli otrzymano cos innego to znaczy ze nie mozna grac online zwracany jest null ktory pozniej sprawia ze gra jest prowadzona w trybie offline
            {
                return null;
            }
        }
        catch (Exception e) {
            System.out.println("Nie można nawiązac poloaczenia");
            System.out.println("error: "+e);
        }
        return null;
    }

}

