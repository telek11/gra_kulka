
package Stale;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.Socket;

/**
 *  Stałe parametry inicjializacyjne wymagane do startu programu
 */

public final class Stale {
    /**
     * Adres pliku config.xml
     */
    public static final String xmlConfigFile = "PlikiKonfiguracyjne\\config.xml";

    /**
     * Zmienna określająca szerokość okna menu głównego
     */
    public static int szerokOknMenuGlow;
    /**
     * Zmienna określająca wysokość okna menu głównego
     */
    public static int wysokOknMenuGlow;
    /**
     * Zmienna określająca tytuł okna głownego
     */
    public static String napisOknaGlow;
    /**
     * Zmienna określająca tekst wyświetlany w oknie przycisku powrotu do menu głownego
     */
    public static String napisNaPrzycPowrDoMenuGlow;
    /**
     * Zmienna określająca droge do xml zawierającego najwyższe wyniki
     */
    public static String xmlPlikNajlepWyn;
    /**
     * Zmienna określająca domyślną liczbę punktów;
     */
    public static int initLiczbePkt;
    /**
     * Zmienna okreslająca początkową liczbę żyć
     */
    public static int initLiczbeZyc;


    /**
     * Zmienna określajśca tekst w etykiecie liczby pozostałych żyć
     */
    public static String napisZycia;
    /**
     * Zmienna określająca tekst w etykiecie najlepszego wyniku
     */
    public static String napisNajlepWyn;

    /**
     * Zmienna określająca iloś FPS
     */
    public static int FPS;

    /**
     * Zmienna określająca czas uśpienia wątku obliczjącego gry
     */
    public static int czasSnuThread;
    /**
     * Teskt pola uruchom ponownie
     */
    public static String napisUruchomPonownie;
    /**
     * Tekst Upłynął czas
     */
    public static String napisUplynalCzas;

    /**
     * Tekst że skonczyly sie zycia
     */
    public static String napisSkonczylySieZycia;
    /**
     * Tekst że gra skonczona
     */
    public static String napisKoniecGry;

    /**
     * Tekst że gra skonczona i gratulacje
     */
    public static String napisKoniecGryInfo;

    /**
     * Text Liczba punktów
     */
    public static String napisLiczbePkt;

    /**
     * Numer pierwszego poziomu w przypadku gry offline
     */
    public static int numerPierwPoziom;

    /**
     * Numer ostatniego poziomu w przypadku gry offline
     */
    public static int numerOstatnPoziom;
    /**
     * Socket serwera
     */
    public static Socket st;


    static{
        parseMenuConfigurationFile();

    }

    private Stale(){}


    /**
     * Metoda do inicjacji pól z pliku konfugracyjnego
     */

    public static void parseMenuConfigurationFile (){
        try {

            File xmlInputFile = new File(xmlConfigFile);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlInputFile);
            doc.getDocumentElement().normalize();



            szerokOknMenuGlow =Integer.parseInt(doc.getElementsByTagName("szerokOknMenuGlow").item(0).getTextContent());
            wysokOknMenuGlow =Integer.parseInt(doc.getElementsByTagName("wysokOknMenuGlow").item(0).getTextContent());
            napisOknaGlow =doc.getElementsByTagName("napisOknaGlow").item(0).getTextContent();
            napisNaPrzycPowrDoMenuGlow =doc.getElementsByTagName("napisNaPrzycPowrDoMenuGlow").item(0).getTextContent();
            xmlPlikNajlepWyn =doc.getElementsByTagName("xmlPlikNajlepWyn").item(0).getTextContent();
            initLiczbePkt=Integer.parseInt(doc.getElementsByTagName("initLiczbePkt").item(0).getTextContent());
            initLiczbeZyc=Integer.parseInt(doc.getElementsByTagName("initLiczbeZyc").item(0).getTextContent());
            napisZycia=doc.getElementsByTagName("napisZycia").item(0).getTextContent();
            napisNajlepWyn=doc.getElementsByTagName("napisNajlepWyn").item(0).getTextContent();
            FPS=Integer.parseInt(doc.getElementsByTagName("FPS").item(0).getTextContent());
            czasSnuThread=Integer.parseInt(doc.getElementsByTagName("czasSnuThread").item(0).getTextContent());
            napisUruchomPonownie=doc.getElementsByTagName("napisUruchomPonownie").item(0).getTextContent();
            napisUplynalCzas=doc.getElementsByTagName("napisUplynalCzas").item(0).getTextContent();
            napisSkonczylySieZycia=doc.getElementsByTagName("napisSkonczylySieZycia").item(0).getTextContent();
            napisKoniecGry= doc.getElementsByTagName("napisKoniecGry").item(0).getTextContent();
            napisLiczbePkt=doc.getElementsByTagName("napisLiczbePkt").item(0).getTextContent();
            napisKoniecGryInfo =doc.getElementsByTagName("napisKoniecGryInfo").item(0).getTextContent();
            numerPierwPoziom=Integer.parseInt(doc.getElementsByTagName("numerPierwPoziom").item(0).getTextContent());
            numerOstatnPoziom=Integer.parseInt(doc.getElementsByTagName("numerOstatnPoziom").item(0).getTextContent());


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda do inicjacji pól z serewera
     * @param serverSocket
     */
    public static void parseMenuConfigurationFileFromServer(Socket serverSocket) {
        try {
            serverSocket.getInputStream().skip(serverSocket.getInputStream().available()); // Znieszczenie(pominiecie) czegokolwiek co bylo w buforze

            OutputStream os = serverSocket.getOutputStream();
            PrintWriter pw = new PrintWriter(os, true);
            pw.println("Pobierz_Ustawienia"); // wysyla do serwera komende Pobierz_Ustawienia

            InputStream is = serverSocket.getInputStream(); // Pobiera strumien wejsciowy z serwera
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String settings = br.readLine(); //  string w ktorym zapisana jest odpowiedz serwera

            System.out.println(settings);

            try {

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(new InputSource(new StringReader(settings))); //Parsowanie stringa zawierajcaego odpoweidz serwera
                doc.getDocumentElement().normalize();                       // Wykorzytsanie Parsera DOM

                szerokOknMenuGlow = Integer.parseInt(doc.getElementsByTagName("szerokOknMenuGlow").item(0).getTextContent());
                wysokOknMenuGlow = Integer.parseInt(doc.getElementsByTagName("wysokOknMenuGlow").item(0).getTextContent());
                napisOknaGlow = doc.getElementsByTagName("napisOknaGlow").item(0).getTextContent();
                napisNaPrzycPowrDoMenuGlow = doc.getElementsByTagName("napisNaPrzycPowrDoMenuGlow").item(0).getTextContent();
                xmlPlikNajlepWyn = doc.getElementsByTagName("xmlPlikNajlepWyn").item(0).getTextContent();
                initLiczbePkt = Integer.parseInt(doc.getElementsByTagName("initLiczbePkt").item(0).getTextContent());
                initLiczbeZyc = Integer.parseInt(doc.getElementsByTagName("initLiczbeZyc").item(0).getTextContent());
                napisZycia = doc.getElementsByTagName("napisZycia").item(0).getTextContent();
                napisNajlepWyn = doc.getElementsByTagName("napisNajlepWyn").item(0).getTextContent();
                FPS = Integer.parseInt(doc.getElementsByTagName("FPS").item(0).getTextContent());
                czasSnuThread = Integer.parseInt(doc.getElementsByTagName("czasSnuThread").item(0).getTextContent());
                napisUruchomPonownie = doc.getElementsByTagName("napisUruchomPonownie").item(0).getTextContent();
                napisUplynalCzas = doc.getElementsByTagName("napisUplynalCzas").item(0).getTextContent();
                napisSkonczylySieZycia = doc.getElementsByTagName("napisSkonczylySieZycia").item(0).getTextContent();
                napisKoniecGry = doc.getElementsByTagName("napisKoniecGry").item(0).getTextContent();
                napisLiczbePkt = doc.getElementsByTagName("napisLiczbePkt").item(0).getTextContent();
                napisKoniecGryInfo = doc.getElementsByTagName("napisKoniecGryInfo").item(0).getTextContent();
                numerPierwPoziom = Integer.parseInt(doc.getElementsByTagName("numerPierwPoziom").item(0).getTextContent());
                numerOstatnPoziom = Integer.parseInt(doc.getElementsByTagName("numerOstatnPoziom").item(0).getTextContent());
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            System.out.println("PLik nie mógł zostać pobrany z serwera");
            System.out.println(e);
        }
    }
}

