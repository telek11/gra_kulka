package Kulka_server;

/**
 * Created by Tomasz Jastrzębski & Dzmitry Astrouski 
 */

/**
 * Klasa przechowuje pare Nazwa_Gracza-Punkty
 */
public class Para {

    /**
     * Nick-Nazwa gracza
     */
    private String name;
    /**
     * Liczba punktów
     */
    private int points;

    /**
     * Konstruktor klasy
     * @param name nick
     * @param points liczba punktów
     */
    public Para(String name, int points){
        this.name=name;
        this.points=points;
    }

    /**
     * Metoda zwracająca nazwe-nick gracza
     * @return nick gracza
     */
    public String getName() {
        return name;
    }

    /**
     * Metoda zwracająca punkty
     * @return punkty gracza
     */
    public int getPoints() {
        return points;
    }
}
