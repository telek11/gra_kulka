package Kulka_server;

/**
 * Created by Tomasz Jastrzębski & Dzmitry Astrouski 
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Klasa Servera
 */
public class Server {
    /**
     * Port serwera
     */
    private int serverPort;

    /**
     * Metoda ustawaiająca port i startująca serwer
     */
    public void runServer() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("src\\PlikiKonfiguracyjne\\IP.txt"));
            serverPort = Integer.parseInt(br.readLine());
            ServerSocket serverSocket = new ServerSocket(serverPort);
            System.out.println("Server gotowy. Czeka na polaczenia...");
            while (true)
            {
                Socket socket = serverSocket.accept();
                new Thread(new ServerWatek(socket)).start();
            }
        }
        catch (IOException e){
            System.out.println("Serwer nie mógł zostać uruchomiony");
            System.err.println(e);
        }
    }

}
