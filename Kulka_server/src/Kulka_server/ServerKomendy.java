package Kulka_server;

/**
 * Created by Tomasz Jastrzębski & Dzmitry Astrouski 
 */

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/**
 * Klasa wykonywujaca komendy przeslane do servera
 */
public class ServerKomendy {

    /**
     * Numer pierwszego poziomu
     */
    private static int pierwszyPoziom=1;
    /**
     * Numer ostatniego poziomu
     */
    private static int ostatniPoziom=2;
    /**
     * Numer klienta, ustawiona wartosc domyslna na 0
     */
    private static int numerKlienta =0;
    /**
     * Tablica nazw poziomow
     */
    private static  String[] listaPoziomow;


    /**
     * Statyczny blok inicializacyjny,
     * ustawia wartosci atrybutow statycznych klasy
     */
    static{
        try {
            BufferedReader br = new BufferedReader(new FileReader("src\\PlikiKonfiguracyjne\\poziomy.txt"));
            pierwszyPoziom=Integer.parseInt(br.readLine());
            ostatniPoziom=Integer.parseInt(br.readLine());
            System.out.println("Pierwszy Poziom: "+pierwszyPoziom);
            System.out.println("Ostatni poziom: "+ostatniPoziom);
        }
        catch (IOException e){
            System.out.println("Plik 'src\\PlikiKonfiguracyjne\\IP.txt' nie mogl zostac otwarty");
            System.err.println(e);
        }
        listaPoziomow=new String[ostatniPoziom];
        System.out.println("Lista dostepnych poziomów");
        for(int i=0;i<ostatniPoziom;i++) {
            listaPoziomow[i] = "level" + (i + 1);
            System.out.println(listaPoziomow[i]);
        }
    }

    /**
     * Metoda obslugujaca zadania od serwera
     * @param command komenda
     * @return odpowied� serwera
     */
    public static String serverAction(String command){
        String serverCommand = command;
        String originalCommand= command;
        System.out.println(command);

        if(command.contains("Pobierz_Poziom:")){
            originalCommand=command;
            serverCommand=("Pobierz_Poziom:");
        }
        if(command.contains("Wynik_Gry:")){
            originalCommand=command;
            serverCommand="Wynik_Gry";
        }

        String serverMessage;
        switch (serverCommand){
            case "Zaloguj":
                serverMessage=login();
                break;
            case "Pobierz_Ustawienia":
                serverMessage=getSettings();
                break;
            case "Pobierz_Informacje":
                serverMessage=getInform();
                break;
            case "Pobierz_Najlepsze_Wyniki":
                serverMessage=getHighScores();
                break;
            case "Pobierz_dostepne_poziomy":
                serverMessage=pobierzDostepnePoziomy();
                break;
            case "Pobierz_Poziom:":
                String str[] = originalCommand.split(":");
                serverMessage=getLevel(Integer.parseInt(str[1]));
                break;
            case "Wynik_Gry":
                String str1[] = originalCommand.split(":");
                String str2[] = str1[1].split("@");
                serverMessage=zaktualizujListeWynikow(str2[0],Integer.parseInt(str2[1]));
                break;
            case "Wyloguj":
                serverMessage=wyloguj();
                break;
            case "Poloczenie_Przerwane":
                serverMessage=poloczeniePrzerwane();
                break;
            default:
                serverMessage="Niepoprawna_Komenda";
        }
        return serverMessage;
    }

    /**
     * Metoda obslugujaca logowanie
     * @return wiadomosc od serwera
     */
    private static String login(){
            String serverMessage;
            serverMessage="Zalogowano klient nr"+numerKlienta+"\n";
            numerKlienta++;
        return serverMessage;
    }

    /**
     * Metoda umoliwiajca pobieranie ustwien w postaci pliku konfiguracyjnego
     * @return odpowiedz serwera
     */
    private static String getSettings(){
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader("src\\PlikiKonfiguracyjne\\plikKonfiguracyjny.xml"))){
            String currentLine;
            while ((currentLine = br.readLine()) != null)
            {
                sb.append(currentLine);
            }
        }
        catch (Exception e){
        }
        return sb.toString();
    }

    /**
     * Metoda umożliwiajaca pobranie najwyzszych wynikoów
     * @return odpowiedz serwera
     */
    private static String getHighScores()
    {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader("src\\PlikiKonfiguracyjne\\highscores.xml"))){
            String currentLine;
            while ((currentLine = br.readLine()) != null)
            {
                sb.append(currentLine);
            }
        }
        catch (Exception e){
        }
        return sb.toString();
    }
    /**
     * Metoda umozliwiajca pobranie zasad
     * @return odpowiedz serwera
     */
    private static String getInform(){
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader("src\\PlikiKonfiguracyjne\\zasady.xml"))){
            String currentLine;
            while ((currentLine = br.readLine()) != null) {
                sb.append(currentLine);
            }
        }
        catch (Exception e){
        }
        return sb.toString();
    }

    /**
     * Metoda aktualizujaca liste wynikow
     * @param nick nick gracza
     * @param points licbza zdobytych punktow
     * @return odpowiedz serwera
     */
    private static String zaktualizujListeWynikow(String nick, int points)
    {
        ArrayList<Para> highscoreList;
        highscoreList = new ArrayList<>();
        highscoreList=loadFromFile(highscoreList);
        String response="Wynik_Gry_zaakceptowany";
        for(int i=0;i<10;i++){
            if(highscoreList.get(i).getPoints()<points){
                response="Wynik_Gry_odrzucony";
                break;
            }
        }
        addScore(highscoreList,nick,points);
        return response;
    }

    /**
     * Metoda dodajaca wynik do listy najlepszych wynikow
     * @param highscoreList lista wynikow
     * @param Name nick gracza
     * @param Points liczba punktow
     */
    private static void addScore(ArrayList<Para> highscoreList,String Name, int Points)
    {
        highscoreList.add(new Para(Name,Points));
        sortList(highscoreList);
        saveHighscoresToFile(highscoreList);
    }

    /**
     * Metoda wczytujaca liste najlepszych wynikow z pliku
     * @param highscoreList lista wynikow
     * @return odpowiedz serwera
     */
    private static ArrayList<Para> loadFromFile(ArrayList<Para> highscoreList) {
        try {
            File xmlInputFile = new File("src\\PlikiKonfiguracyjne\\highscores.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlInputFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("ScoreID");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    highscoreList.add(new Para(eElement.getElementsByTagName("name").item(0).getTextContent(), Integer.parseInt(eElement.getElementsByTagName("score").item(0).getTextContent())));
                }
            }
            return highscoreList;
        }
        catch (FileNotFoundException e) {
            System.out.println(e.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return highscoreList;
    }
    /**
     * Metoda zapisujaca wyniki do pliku
     * @param highscoreList  lista wynikow
     */
    private static void saveHighscoresToFile(ArrayList<Para> highscoreList){
        try{
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("highscores");
            doc.appendChild(rootElement);
            for(int i=0;i<10;i++) {
                if(highscoreList.size()<=i)
                    break;
                Element ScoreID = doc.createElement("ScoreID");
                rootElement.appendChild(ScoreID);
                ScoreID.setAttribute("id", Integer.toString(i));

                Element name = doc.createElement("name");
                name.appendChild(doc.createTextNode(highscoreList.get(i).getName()));
                ScoreID.appendChild(name);

                Element score = doc.createElement("score");
                score.appendChild(doc.createTextNode(Integer.toString(highscoreList.get(i).getPoints())));
                ScoreID.appendChild(score);
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("src\\PlikiKonfiguracyjne\\highscores.xml"));
            transformer.transform(source, result);

        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }
    /**
     * Metoda sortujaca liste wynikow
     * @param highscoreList lista wynikow
     */
    private static void sortList(ArrayList<Para> highscoreList) {
        Collections.sort(highscoreList, new Comparator<Para>() {
            @Override
            public int compare(Para pair, Para t1)
            {
                if (pair.getPoints() < t1.getPoints()) {
                    return 1;
                }
                if (pair.getPoints() > t1.getPoints()) {
                    return -1;
                } else
                    return 0;
            }
        });
    }

    /**
     * Metoda pboierajaca zakres poziomow
     * @return odpowiedz serwera
     */
    private static String pobierzDostepnePoziomy(){
        String message = "Dostepne_poziomy: FIRST: "+pierwszyPoziom+"; LAST: "+ostatniPoziom;
        return message;
    }

    /**
     * Metoda pobierajaca dane poziomuy
     * @param numerPoziomu numer poziomu
     * @return odpowiedz serwera
     */
    private static String getLevel(int numerPoziomu){
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader("src\\PlikiKonfiguracyjne\\"+listaPoziomow[numerPoziomu-1]+".xml"))){

            String currentLine;
            while ((currentLine = br.readLine()) != null) {
                sb.append(currentLine);
            }
        }
        catch (Exception e){
        }
        return sb.toString();
    }

    /**
     * Metoda odpowadajaca za wylogowanie
     * @return odpowiedz serwera
     */
    private static String wyloguj(){
        return "Wylogowano";
    }

    /**
     * Metoda odpowiadajaca za przerwanie polaczenia
     * @return odpowiedz serwera
     */
    private static String poloczeniePrzerwane(){
        return "Przereij_polaczenie";
    }


}
