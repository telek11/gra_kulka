package Kulka_server;

/**
 * Created by   on 2017-01-09.
 */

import Kulka_server.ServerKomendy;

import java.io.*;
import java.net.Socket;

/**
 * Klasa reprezentujaca watek servera
 */
public class ServerWatek implements Runnable{
    /**
     * Socket
     */
    private Socket socket;
    /**
     * Konstruktor klasy
     * @param socket socket
     */
    public ServerWatek(Socket socket){
        this.socket=socket;
    }

    /**
     * Metoda obsługująca wszystkie zdarzenia pomiedzy klientem i serwerem
     */
    @Override
    public void run() {
        try {
            while (true) {
                InputStream inputStream = socket.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                OutputStream os = socket.getOutputStream();
                PrintWriter pw = new PrintWriter(os, true);
                String fromClient = br.readLine();
                if(fromClient!=null)
                {
                    System.out.println("FROM_CLIENT: " + fromClient);
                    String serverMessage = ServerKomendy.serverAction(fromClient);
                    if(serverMessage=="Przerwij_poloczenie")
                    {
                        socket.close();
                    }
                        pw.println(serverMessage);
                        pw.flush();
                        System.out.println("TO_CLIENT: " + serverMessage);
                        if (serverMessage == "Wylogowano")
                        {
                            socket.close();
                        }
                }
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
