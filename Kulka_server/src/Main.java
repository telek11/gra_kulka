/**
 * Created by Tomasz Jastrzębski & Dzmitry Astrouski 
 */

import Kulka_server.Server;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Klasa uruchamiajaca server
 */
public class Main extends JFrame implements ActionListener {

    public Main()
    {
        setTitle("SERVER");
        setSize(200,100);

        JButton WylaczButton = new JButton("WYLACZ SERVER");
        WylaczButton.addActionListener(this);
        WylaczButton.setActionCommand("Wylacz");
        add(WylaczButton);
    }

    /**
     * Metoda głowna serwera która uruchamia serwer oraz
     * tworzy okno sygnalizujące jego pracę
     * @param args pparametr funcji main
     */
    public static void main(String[] args)
    {
        Main oknoServera = new Main();
        oknoServera.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        oknoServera.setVisible(true);

        new Server().runServer();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        String command = e.getActionCommand();
        switch (command){
            case "Wylacz":
                System.exit(0);
                break;
        }
    }
}
